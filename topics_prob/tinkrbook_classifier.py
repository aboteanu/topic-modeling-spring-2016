'''
Note: Copy the file to root folder and execute <python tinkrbook_classifier.py>

'''

import os
import re
import string
from gensim import corpora, models, similarities 
from nltk.stem.snowball import SnowballStemmer
from nltk.stem.wordnet import WordNetLemmatizer
from nltk import pos_tag,word_tokenize,sent_tokenize
from nltk.metrics import edit_distance
from nltk.corpus import stopwords
import numpy as np
import topics_semnet.conceptnet_api.conceptnet_api as conceptnet_api
import topics_semnet.wordnet_api.wordnet_api as wordnet_api
import topics_semnet.cyc_api.cyc_api as cyc_api
import topics_semnet.topics.generate_topics_from_vocab as gen_topics
from topics_semnet.conceptnet_api.conceptnet_api import node_expansion as concept_node_expansion
from topics_semnet.conceptnet_api.divisi_sim import concept_pair_sim
from topics_semnet.wordnet_api.wordnet_api import node_expansion as wordnet_node_expansion
from topics_semnet.wordnet_api.wordnet_path_sim import wordnet_pair_sim as wordnet_pair_sim
from topics_semnet.cyc_api.cyc_api import node_expansion as cyc_node_expansion
import enchant
import pickle
import topics_semnet.topics.topics as ClassTopics


class SpellingReplacer(object):
  def __init__(self, dict_name='en', max_dist=2):
    self.spell_dict = enchant.Dict(dict_name)
    self.max_dist = max_dist

  def replace(self, word):
    if self.spell_dict.check(word):
      return word
    suggestions = self.spell_dict.suggest(word)

    if suggestions and edit_distance(word, suggestions[0]) <= self.max_dist:
      return suggestions[0]
    else:
      return word

def lemmatize(model,token,tag):
    if tag[0].lower() in ['n', 'v']:
        return model.lemmatize(token, tag[0].lower())
    return token

def tokenize_and_stem(text):
	tokens = [word for sent in sent_tokenize(text) for word in word_tokenize(sent)]
	filtered_tokens = []
	stems=[]
	final_stems=[]
	final_corpus = []
	tagged_tokens = []
	for token in tokens:
		if re.search('[a-zA-Z]', token):
			filtered_tokens.append(token)
	for t in filtered_tokens:
		try:
			temp = stemmer.stem(t)
			stems.append(lemmatizer.lemmatize(temp,pos='v'))
			temp1 = lemmatizer.lemmatize(temp,pos='n')
			if temp1 not in stems:
				stems.append(temp1)
		except UnicodeDecodeError:
			print ("UnicodeDecodeError:" + str(t)) 
	return stems

def strip_proppers(text):
    tokens = [word for sent in sent_tokenize(text) for word in word_tokenize(sent) if word.islower()]
    return "".join([" "+i if not i.startswith("'") and i not in string.punctuation else i for i in tokens]).strip()


class Character(object):
	def __init__(self,name):
		self.name = name
		self.dialogue = []
		self.lda_topics=[]
		self.dict=[]
		self.bow=[]
		self.conceptnet_topics=[]
		self.wordnet_topics=[]
		self.opencyc_topics=[]
		self.features=[]
	def add_dialogues(self, new_review):
		self.dialogue.append(new_review)
	def print_character(self):
		print ("Name: " + self.name)
		if(len(self.bow)!=0):
			print ("Bag of Words: " + str(self.bow))
		if(len(self.lda_topics)!=0):
			print ("LDA Topics: " + str(self.lda_topics)) 
		if(len(self.conceptnet_topics)!=0):
			print("ConceptNet Topics: " + str(self.conceptnet_topics))
		if(len(self.wordnet_topics)!=0):
			print("WordNet Topics:" + str(self.wordnet_topics))
		if(len(self.opencyc_topics)!=0):
			print("OpenCyc Topics: " + str(self.opencyc_topics))
		print("Features: " + str(self.features))	
	def generate_dictionary(self):
		preprocess = [strip_proppers(doc) for doc in self.dialogue]
		tokenized_text = [tokenize_and_stem(text) for text in preprocess]
		texts = [[word for word in text if word not in stopwords] for text in tokenized_text]
		cTexts = []
		for text in texts:
			line = []
			for word in text:
				if(len(word)>3 and len(conceptnet_api.conceptnet_api(word))!=1 and len(wordnet_api.node_expansion(word))!=0):
					line.append(replacer.replace(word))
			cTexts.append(line)
		self.dictionary = corpora.Dictionary(cTexts)
		for keyid in self.dictionary.keys():
			try:
				self.bow.append(str(self.dictionary[keyid]))
			except UnicodeEncodeError:
				print("UnicodeEncodeError" + str(self.dictionary[keyid]))
		self.dictionary.filter_extremes(no_below=1, no_above=0.8)
		self.dictionary.compactify()
		self.corpus = [self.dictionary.doc2bow(text) for text in cTexts]

	def generate_topics_using_lda(self):
		if (len(self.dictionary.keys())<5):
			print (self.name + " has less than unique tokens.. skipping lda")
			print ("Dialogues:  " + str(self.dialogue))
			return 
		lda = models.LdaModel(self.corpus, num_topics=5, 
                            id2word=self.dictionary, 
                            update_every=5, 
                            chunksize=10000, 
                            passes=100)
		topics_matrix = lda.show_topics(num_topics=5, num_words=10, log=False, formatted=True)
		topics_matrix = np.array(topics_matrix)
		topic_words = topics_matrix[:,:]
		words = []
		for i in topic_words:
			words = re.findall('\*\w+',i[1])
			topic = []
			for word in words:
				topic.append(replacer.replace(str(word[1:])))
			self.lda_topics.append(topic)
    
	def generate_topics_using_conceptnet(self):
		stopics=gen_topics.generate_topics_from_vocab(self.bow,pair_sim=concept_pair_sim, node_expansion=concept_node_expansion)
		for lists in stopics:
			lists = list(lists)
			if len(lists)>1:
				self.conceptnet_topics.append(lists)
	def generate_topics_using_wordnet(self):
		stopics = gen_topics.generate_topics_from_vocab(self.bow,pair_sim=wordnet_pair_sim, node_expansion=wordnet_node_expansion)        
		for lists in stopics:
			lists = list(lists)
			if len(lists)>1:
				self.wordnet_topics.append(lists)
	def generate_topics_using_opencyc(self):
		stopics = gen_topics.generate_topics_from_vocab(self.bow,pair_sim=wordnet_pair_sim, node_expansion=cyc_node_expansion)
		for lists in stopics:
			lists = list(lists)
			if len(lists)>1:
				self.opencyc_topics.append(lists)	
	def generate_topics_using_semnet(self):
		if (len(self.dictionary.keys())==0):
			print (self.name + " has no unique tokens.. skipping topics_semnet")
			print ("Reviews:  " + str(self.dialogue))
			return 
		print ("Generating ConceptNet Topics:")
		self.generate_topics_using_conceptnet()
		print ("Generating WordNet Topics:")
		self.generate_topics_using_wordnet()
		print ("Generating OpenCyc Topics:")
		self.generate_topics_using_opencyc() 


'''
read_corpus: Function to read the corpus from directory based on the extension type
	Reads the files with given extension from the directory and 
	results a list of documents read
Input: 
	file_path: absolute path to the corpus directory 
	extn: 	type of file (like text,csv,json)
Returns: 
	Returns a python list of documents read
'''

def read_corpus(file_path,extn):
	files = []
	for name in os.listdir(file_path):
		complete_path = file_path+'/'+name
		if(name.endswith('.'+extn) and os.path.isfile(complete_path)):
			files.append(complete_path)

	doc_corpus = []
	prev_name = "Unknown"
	character_obj = Character(prev_name)
	dialogue_map[prev_name] = character_obj
	for filename in files:
		dfile = open(filename,'r')
		doc_set = dfile.read()
		prev_name = "Unknown"
		doc_set = doc_set.split('\n')
		for line in doc_set: 			
			name_match = re.match('\s*[a-zA-Z]+:\s*',line) 
			if name_match:
				name = name_match.group()
				name = name.replace(" ","")
				name = name.replace("\t","")
				name = name.replace(":","")
				temp = re.match('\d+',name)
				if name not in dialogue_map.keys():
					character_obj = Character(name)
					dialogue_map[name] = character_obj					
				else: 
					character_obj = dialogue_map[name]	
				prev_name = name
			else: 
				if prev_name: 
					character_obj = dialogue_map[prev_name]

			#Remove name
			line = re.sub('\s*[a-zA-Z]+:\s*', '',line)
			#Remove contents within [...]
			line = re.sub('\[.*?\]','',line)
			#print (line)
			if character_obj:
				character_obj.add_dialogues(line)
			doc_corpus.append(line)
		dfile.close()
	return doc_corpus


def compare_topics(topic1,topic2,pair_sim):
	sim = 0.0
	for word1 in topic1:
		for word2 in topic2:
			sim+=ClassTopics.Topics.topic_similarity3(word1,word2,pair_sim)
	return (sim/(len(topic1)*len(topic2)))

def filter_features(topic1,topic2,avg_sim,pair_sim):
	retList = []
	for word1 in topic1:
		for word2 in topic2:
			if (compare_topics(word1,word2,pair_sim)>avg_sim):
				if word2 not in retList:
					retList.append(word2)
				if word1 not in retList:
					retList.append(word1)
	return retList

def get_features(lda_topics,wordnet_topics,pair_sim):
	sim=0.0
	featureList=[]
	for lda_topic in lda_topics:
		for wordnet_topic in wordnet_topics:
			sim+=compare_topics(lda_topic,wordnet_topic,pair_sim)
	avg_sim = sim/(len(lda_topics) * len(wordnet_topics))
	print ("Average Similarity:" + str(avg_sim))
	for lda_topic in lda_topics:
		for wordnet_topic in wordnet_topics:
			retList = filter_features(lda_topic,wordnet_topic,avg_sim,pair_sim)
			for word in retList:
				if word not in featureList:
					featureList.append(word)	
	print ("Features: " + str(featureList))
	return featureList

'''
Global Variables:
'''

dialogue_map = {}
stemmer = SnowballStemmer("english")
replacer = SpellingReplacer()
stopwords = stopwords.words('english')
wordnet_tags = ['n','v']
lemmatizer = WordNetLemmatizer()

def main():
	doc_corpus = read_corpus("data/tinkrbook","txt")
	for idx in dialogue_map.keys():
		c_obj = dialogue_map[idx]     
		c_obj.generate_dictionary()
		c_obj.generate_topics_using_lda()
		c_obj.generate_topics_using_semnet()
	for keyid in dialogue_map.keys():
		c_obj = dialogue_map[keyid]	
		c_obj.print_character()
		if (len(c_obj.lda_topics)!=0):
			if(len(c_obj.wordnet_topics)!=0):
				print("Features from LDA & Wordnet: ")
				c_obj.features.append(get_features(c_obj.lda_topics,c_obj.wordnet_topics,wordnet_pair_sim))
			if(len(c_obj.opencyc_topics)!=0):
				print("Features from LDA & OpenCyc ")
				c_obj.features.append(get_features(c_obj.lda_topics,c_obj.opencyc_topics,wordnet_pair_sim)) 
			if(len(c_obj.conceptnet_topics)!=0):
				print("Features from ConceptNet: ")
				c_obj.features.append(get_features(c_obj.lda_topics,c_obj.conceptnet_topics,concept_pair_sim)) 
'''
Code to directly read character object from pkl file 
	pkl_file = open('tinkerbook-dataset-lda-wordnet-opencyc.pkl', 'rb')
	pkl_file1=open('tinkerbook-dataset-conceptnet.pkl','rb')
	for idx in range(0,34):
		c_obj = pickle.load(pkl_file)
		c_obj1 = pickle.load(pkl_file1)
		new_c_obj = Character(c_obj.name)
		new_c_obj.lda_topics = c_obj.lda_topics
		new_c_obj.conceptnet_topics = c_obj1.conceptnet_topics
		new_c_obj.wordnet_topics = c_obj.wordnet_topics
		new_c_obj.opencyc_topics = c_obj.opencyc_topics
		new_c_obj.bow = c_obj.bow
		new_c_obj.dialogue = c_obj.dialogue
		new_c_obj.dict = c_obj.dict		
		dialogue_map[c_obj.name]=new_c_obj

	pkl_file.close()
	pkl_file1.close()
'''	


if __name__=="__main__":
    main()