'''
Download yelp academic dataset and place it in data/yelp folder.
Note: Copy the file to root folder and execute <python yelp_classifier.py>

'''
import os
import re
import string
import json
from gensim import corpora, models, similarities 
from nltk.stem.snowball import SnowballStemmer
from nltk.stem.wordnet import WordNetLemmatizer
from nltk import pos_tag,word_tokenize,sent_tokenize
from nltk.metrics import edit_distance
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import numpy as np
import topics_semnet.conceptnet_api.conceptnet_api as conceptnet_api
import topics_semnet.wordnet_api.wordnet_api as wordnet_api
import topics_semnet.cyc_api.cyc_api as cyc_api
import topics_semnet.topics.generate_topics_from_vocab as gen_topics
from topics_semnet.conceptnet_api.conceptnet_api import node_expansion as concept_node_expansion
from topics_semnet.conceptnet_api.divisi_sim import concept_pair_sim
from topics_semnet.wordnet_api.wordnet_api import node_expansion as wordnet_node_expansion
from topics_semnet.wordnet_api.wordnet_path_sim import wordnet_pair_sim as wordnet_pair_sim
from topics_semnet.cyc_api.cyc_api import node_expansion as cyc_node_expansion
import enchant
import pickle
import topics_semnet.topics.topics as ClassTopics


class SpellingReplacer(object):
  def __init__(self, dict_name='en', max_dist=2):
    self.spell_dict = enchant.Dict(dict_name)
    self.max_dist = max_dist

  def replace(self, word):
    if self.spell_dict.check(word):
      return word
    suggestions = self.spell_dict.suggest(word)

    if suggestions and edit_distance(word, suggestions[0]) <= self.max_dist:
      return suggestions[0]
    else:
      return word
'''
Global Variables 
'''
stemmer = SnowballStemmer("english")
replacer = SpellingReplacer()
stopwords = stopwords.words('english')
wordnet_tags = ['n','v']
lemmatizer = WordNetLemmatizer()
business_map = {}
category_map ={}

def lemmatize(model,token,tag):
    if tag[0].lower() in ['n', 'v']:
        return model.lemmatize(token, tag[0].lower())
    return token

def tokenize_and_stem(text):
    tokens = [word for sent in sent_tokenize(text) for word in word_tokenize(sent)]
    filtered_tokens = []
    stems=[]
    final_stems=[]
    final_corpus = []
    tagged_tokens = []
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    for t in filtered_tokens:
        try:
            temp = stemmer.stem(t)
            stems.append(lemmatizer.lemmatize(temp,pos='v'))
            temp1 = lemmatizer.lemmatize(temp,pos='n')
            if temp1 not in stems:
                stems.append(temp1)
        except UnicodeDecodeError:
            print (t) 
    return stems

def strip_proppers(text):
    tokens = [word for sent in sent_tokenize(text) for word in word_tokenize(sent) if word.islower()]
    return "".join([" "+i if not i.startswith("'") and i not in string.punctuation else i for i in tokens]).strip()


class Category(object):
    def __init__(self,category_id):
        self.category_id = category_id
        self.category_tags = []
        self.business_ids = []
        self.reviews = []
        self.conceptnet_topics = []
        self.wordnet_topics = []
        self.opencyc_topics = []
        self.lda_topics = []
        self.bow = []
        self.cb_reviews = []
        self.pca_reviews = []
        self.features=[]
    def print_category(self):
        print ("Category Id: " + str(self.category_id))
        print ("Category Tags: " + str(self.category_tags))
        print ("Category Business Ids: " + str(self.business_ids))
        print ("Category Reviews: " + str(self.reviews))
        if(len(self.lda_topics)==0):
            print ("No LDA Topics as Dictionary is empty")
        else:
            print ("LDA Topics: " + str(self.lda_topics))
        if(len(self.conceptnet_topics)==0):
            print ("No ConceptNet Topics as Dictionary is empty")
        else:
            print ("ConceptNet Topics: " + str(self.conceptnet_topics))
        if(len(self.wordnet_topics)==0):
            print ("No WordNet Topics as Dictionary is empty")
        else:
            print ("WordNet Topics: " + str(self.wordnet_topics))
        if(len(self.opencyc_topics)==0):
            print ("No OpenCyc Topics as Dictionary is empty")
        else:
            print ("OpenCyc Topics: " + str(self.opencyc_topics))
        print ("Reviews: " + str(self.reviews))
  
    def add_reviews(self,new_review):
        self.reviews.append(new_review)
    def generate_dictionary(self):
        print("preprocess:")
        preprocess = [strip_proppers(doc) for doc in self.reviews]
        print ("tokenized_text:")
        tokenized_text = [tokenize_and_stem(text) for text in preprocess]
        print ("texts:")
        texts = [[word for word in text if word not in stopwords] for text in tokenized_text]
        cTexts = []
        print ("Spell correction:")
        for text in texts:
            line = []
            print (len(text))
            for word in text:
                word =word.replace("'","")
                try:
                    if(len(word)>3 and len(conceptnet_api.conceptnet_api(word))!=1 and len(wordnet_api.node_expansion(word))!=0):
                        line.append(replacer.replace(word))
                except UnicodeEncodeError:
                    print (word)
            cTexts.append(line)
        print ("building dictionary")
        self.dictionary = corpora.Dictionary(cTexts)
        for keyid in self.dictionary.keys():
            try:    
                self.bow.append(str(self.dictionary[keyid]))
            except UnicodeEncodeError:
                print (keyid)
        self.dictionary.filter_extremes(no_below=1, no_above=0.8)
        self.dictionary.compactify()
        self.corpus = [self.dictionary.doc2bow(text) for text in cTexts]
        print (self.bow)

    def generate_topics_using_lda(self):
        if (len(self.dictionary.keys()) ==0):
            print (self.category_id + " has no unique tokens.. skipping lda")
            print ("Reviews:  " + str(self.reviews))
            return 

        lda = models.LdaModel(self.corpus, num_topics=5, 
                            id2word=self.dictionary, 
                            update_every=5, 
                            chunksize=10000, 
                            passes=100)
        topics_matrix = lda.show_topics(num_topics=5, num_words=20, log=False, formatted=True)
        topics_matrix = np.array(topics_matrix)
        topic_words = topics_matrix[:,:]
        words = []
        for i in topic_words:
            words = re.findall('\*\w+',i[1])
            topic = []
            for word in words:
                topic.append(word[1:])
            self.lda_topics.append(topic)

    def generate_topics_using_conceptnet(self):
        stopics = gen_topics.generate_topics_from_vocab(self.bow,pair_sim=concept_pair_sim, node_expansion=concept_node_expansion)
        print (type(stopics))
        
        for lists in stopics:
            lists = list(lists)
            self.conceptnet_topics.append(lists)
 
    def generate_topics_using_wordnet(self):
        stopics = gen_topics.generate_topics_from_vocab(self.bow,pair_sim=wordnet_pair_sim, node_expansion=wordnet_node_expansion)
        print (type(stopics))
        
        for lists in stopics:
            lists = list(lists)
            self.wordnet_topics.append(lists)

    def generate_topics_using_opencyc(self):
        stopics = gen_topics.generate_topics_from_vocab(self.bow,pair_sim=wordnet_pair_sim, node_expansion=cyc_node_expansion)
        print (type(stopics))
        
        for lists in stopics:
            lists = list(lists)
            self.opencyc_topics.append(lists)

    def generate_topics_using_semnet(self):
        if (len(self.dictionary.keys()) ==0):
            print (self.name + " has no unique tokens.. skipping topics_semnet")
            print ("Reviews:  " + str(self.reviews))
            return 
        print ("Generating ConceptNet Topics:")
        self.generate_topics_using_conceptnet()
        print ("Generating WordNet Topics:")
        self.generate_topics_using_wordnet()
        print ("Generating OpenCyc Topics:")
        self.generate_topics_using_opencyc()    

class business(object):

    def __init__(self,business_id,category,name):
        self.business_id = business_id
        self.category = category
        self.reviews = []
        self.name = name
        self.conceptnet_topics = []
        self.wordnet_topics = []
        self.opencyc_topics = []
        self.lda_topics = []
        self.bow = []
        self.features=[]

    def add_reviews(self, new_review):
        self.reviews.append(new_review)

    def print_business(self):
        print ("Business_ID: " + self.business_id)
        print ("Name: " + self.name)
        print ("Category: " + str(self.category))
        if(len(self.bow)!=0):
            print ("Bag of Words: " + str(self.bow))
        if(len(self.lda_topics)!=0):
            print ("LDA Topics: " + str(self.lda_topics))
        if(len(self.conceptnet_topics)!=0):
            print ("ConceptNet Topics: " + str(self.conceptnet_topics))
        if(len(self.wordnet_topics)!=0):
            print ("WordNet Topics: " + str(self.wordnet_topics))
        if(len(self.opencyc_topics)!=0):
            print ("OpenCyc Topics: " + str(self.opencyc_topics))
        print("Features: " + str(self.features))
        #print ("Reviews: " + str(self.reviews))

    def generate_dictionary(self):
        print (len(self.reviews))
        print("preprocess:")
        preprocess = [strip_proppers(doc) for doc in self.reviews]
        print ("tokenized_text:")
        tokenized_text = [tokenize_and_stem(text) for text in preprocess]
        print ("texts:")
        texts = [[word for word in text if word not in stopwords] for text in tokenized_text]
        cTexts = []
        print ("spell correction:")
        for text in texts:
            line = []
            for word in text:
                try:
                    if(len(word)>3 and len(conceptnet_api.conceptnet_api(word))!=1 and len(wordnet_api.node_expansion(word))!=0):
                        line.append(replacer.replace(word))
                except UnicodeEncodeError:
                    print (word)
            cTexts.append(line)
        self.dictionary = corpora.Dictionary(cTexts)
        for keyid in self.dictionary.keys():
            try:
                self.bow.append(str(self.dictionary[keyid]))
            except UnicodeEncodeError:
                print (self.dictionary[keyid])
        self.dictionary.filter_extremes(no_below=1, no_above=0.8)
        self.dictionary.compactify()
        self.corpus = [self.dictionary.doc2bow(text) for text in cTexts]
        print (self.bow)
    def generate_topics_using_lda(self):
        if (len(self.dictionary.keys())<10):
            print (self.name + " has less unique tokens.. skipping lda")
            print ("Dialogues:  " + str(self.reviews))
            return 

        lda = models.LdaModel(self.corpus, num_topics=5, 
                            id2word=self.dictionary, 
                            update_every=5, 
                            chunksize=10000, 
                            passes=100)
        topics_matrix = lda.show_topics(num_topics=5, num_words=10, log=False, formatted=True)
        topics_matrix = np.array(topics_matrix)
        topic_words = topics_matrix[:,:]
        words = []
        for i in topic_words:
            words = re.findall('\*\w+',i[1])
            topic = []
            for word in words:
                topic.append(word[1:])
            self.lda_topics.append(topic)  

    def generate_topics_using_conceptnet(self):
        stopics = gen_topics.generate_topics_from_vocab(self.bow,pair_sim=concept_pair_sim, node_expansion=concept_node_expansion)
        print (type(stopics))
        for lists in stopics:
            lists = list(lists)
            if(len(lists)>1):
                self.conceptnet_topics.append(lists)
 
    def generate_topics_using_wordnet(self):
        stopics = gen_topics.generate_topics_from_vocab(self.bow,pair_sim=wordnet_pair_sim, node_expansion=wordnet_node_expansion)
        print (type(stopics))
        for lists in stopics:
            lists = list(lists)
            if(len(lists)>1):
                self.wordnet_topics.append(lists)
    def generate_topics_using_opencyc(self):
        stopics = gen_topics.generate_topics_from_vocab(self.bow,pair_sim=wordnet_pair_sim, node_expansion=cyc_node_expansion)
        print (type(stopics))
        for lists in stopics:
            lists = list(lists)
            if(len(lists)>1):
                self.opencyc_topics.append(lists)

    def generate_topics_using_semnet(self):
        if (len(self.dictionary.keys()) ==0):
            print (self.name + " has no unique tokens.. skipping topics_semnet")
            print ("Reviews:  " + str(self.reviews))
            return 
        print ("Generating ConceptNet Topics:")
        self.generate_topics_using_conceptnet()
        print ("Generating WordNet Topics:")
        self.generate_topics_using_wordnet()
        print ("Generating OpenCyc Topics:")
        self.generate_topics_using_opencyc()    

def build_business_map():
        with open('data/yelp/yelp_academic_dataset_business.json','r') as f:
            for line in f:
                text = {}
                text = json.loads(line)
                business_obj = business(text['business_id'],text['categories'],text['name'])
                business_map[text['business_id']] = business_obj
        with open('data/yelp/yelp_academic_dataset_review.json','r') as f:
            for line in f:
                text = {}
                text = json.loads(line)
                business_map[text['business_id']].add_reviews(text['text'])
    
def build_category_map(business_map):
    business_array=[]
    category_array=[]
    print ("Build Category Map: " + str(len(business_map.keys())))
    for idx in business_map.keys():
        b_obj = business_map[idx] 
        business_array.append(b_obj.business_id)
        strCategory = ""
        for category in b_obj.category:
            strCategory += str(category) + " "
        category_array.append(strCategory)
    vectorizer=TfidfVectorizer(min_df=1, max_df=0.9, stop_words='english', decode_error='ignore')
    vectorized=vectorizer.fit_transform(category_array)

    print ("Shape of vectorized:")
    print(type(vectorized))

    km=KMeans(n_clusters=100, init='k-means++',n_init=10)
    y_pred = km.fit_predict(vectorized)

    print ("Post k-means clustering:")
    for idx in range(0,len(business_array)):
        if y_pred[idx] not in category_map.keys():
            category_obj = Category(y_pred[idx])
            cList = []
            cList.append(category_array[idx])
            category_obj.category_tags=cList
            bList=[]
            bList.append(business_array[idx])
            category_obj.business_ids=bList
            category_map[y_pred[idx]]=category_obj
        else:
            c_obj = category_map[y_pred[idx]]
            cList = c_obj.category_tags
            cList.append(category_array[idx])
            category_obj.category_tags=cList
            bList = c_obj.business_ids
            bList.append(business_array[idx])
            category_obj.business_ids=bList
            category_map[y_pred[idx]]=category_obj

def generate_category_reviews(business_map,category_map):
    vectorizer=TfidfVectorizer(min_df=1, max_df=0.9, stop_words='english', decode_error='ignore')
    pca = PCA(n_components=100)
    for cat in category_map.keys():
        print ("category id: " + str(cat))
        c_obj = category_map[cat]
        print ("# business ids:" + str(len(c_obj.business_ids)))
        b_reviews = []
        for bid in c_obj.business_ids:
            b_obj = business_map[bid]
            for review in b_obj.reviews:
                b_reviews.append(review)
        print(len(b_reviews))
        c_obj.reviews=b_reviews[:10000]

def compare_topics(topic1,topic2,pair_sim):
    sim = 0.0
    for word1 in topic1:
        for word2 in topic2:
            sim+=ClassTopics.Topics.topic_similarity3(word1,word2,pair_sim)
    return (sim/(len(topic1)*len(topic2)))

def filter_features(topic1,topic2,avg_sim,pair_sim):
    retList = []
    for word1 in topic1:
        for word2 in topic2:
            if (compare_topics(word1,word2,pair_sim)>avg_sim):
                if word2 not in retList:
                    retList.append(word2)
                if word1 not in retList:
                    retList.append(word1)
    return retList

def get_features(lda_topics,wordnet_topics,pair_sim):
    sim=0.0
    featureList=[]
    for lda_topic in lda_topics:
        for wordnet_topic in wordnet_topics:
            #print (lda_topic,wordnet_topic)
            sim+=compare_topics(lda_topic,wordnet_topic,pair_sim)
    avg_sim = sim/(len(lda_topics) * len(wordnet_topics))
    print ("Average Similarity:" + str(avg_sim))
    for lda_topic in lda_topics:
        for wordnet_topic in wordnet_topics:
            retList = filter_features(lda_topic,wordnet_topic,avg_sim,pair_sim)
            for word in retList:
                if word not in featureList:
                    featureList.append(word)    
    print ("Features: " + str(featureList))
    return featureList

def main():
    build_business_map()
    build_category_map(business_map)
    generate_category_reviews(business_map,category_map)  
    for idx in category_map.keys():
        c_obj = category_map[idx] 
        c_obj.generate_dictionary()    
        c_obj.generate_topics_using_lda()
        c_obj.generate_topics_using_semnet()
        c_obj.print_category()

'''
Code to generate features based on business id: 

    #build_business_map()
    #output_step1 = open('yelp-business-map.pkl','wb')
    #pickle.dump(business_map,output_step1)
    #output_step1.close()
    pkl_file = open('yelp-business-map.pkl', 'rb')
    business_map=pickle.load(pkl_file)
    pkl_file.close()
    output = open('yelp-business-dataset-features-5.pkl', 'wb')
    count=1 
    for idx in business_map.keys():
        if count<5:
            new_c_obj = business_map[idx]
            c_obj = business(new_c_obj.business_id,new_c_obj.category,new_c_obj.name)
            c_obj.reviews = new_c_obj.reviews
            print("Building Dictionary")
            c_obj.generate_dictionary()
            print ("Building LDA topics:")    
            c_obj.generate_topics_using_lda()
            c_obj.generate_topics_using_semnet()
            c_obj.print_business()
            if (len(c_obj.lda_topics)!=0):
                if(len(c_obj.wordnet_topics)!=0):
                    print("Features from LDA & Wordnet: ")
                    c_obj.features.append(get_features(c_obj.lda_topics,c_obj.wordnet_topics,wordnet_pair_sim))
                if(len(c_obj.opencyc_topics)!=0):
                    print("Features from LDA & OpenCyc ")
                    c_obj.features.append(get_features(c_obj.lda_topics,c_obj.opencyc_topics,wordnet_pair_sim)) 
                if(len(c_obj.conceptnet_topics)!=0):
                    print("Features from ConceptNet: ")
                    c_obj.features.append(get_features(c_obj.lda_topics,c_obj.conceptnet_topics,concept_pair_sim)) 
            count=count+1
            pickle.dump(c_obj, output)
    output.close()   
''' 
if __name__=="__main__":
    main()

