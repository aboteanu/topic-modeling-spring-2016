import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation
from numpy import loadtxt
import nltk
from nltk import pos_tag,word_tokenize
from nltk.stem import PorterStemmer
from nltk.stem.wordnet import WordNetLemmatizer

n_topics = 50
#no of topics to be extracted 

n_top_words = 20  
# Top words in each topic

wordnet_tags = ['n','v']

def lemmatize(model,token,tag):
    if tag[0].lower() in ['n', 'v']:
        return model.lemmatize(token, tag[0].lower())
    return token

#Function to print topics extracted
def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        print("Topic #%d:" % topic_idx)
        print(" ".join([feature_names[i]
                        for i in topic.argsort()[:-n_top_words - 1:-1]]))
    print()

#Reading a hardcoded corpus of csv file
def read_corpus():
    #Read the csv directly 
    data= pd.read_csv('../data/Reviews.csv',index_col=0)
    text_full = data['Text']
#Hack to return a subset for now
    text = text_full[:1000]
    lemmatizer = WordNetLemmatizer()
    tagged_corpus = [pos_tag(word_tokenize(document)) for document in text]

    final_corpus = []
    for document in tagged_corpus:
        for token, tag in document:
            final_corpus.append(lemmatize(lemmatizer,token,tag))

    return final_corpus

#Extracting bag of words from corpus
def topic_model(final_corpus):
    print("Extracting tf features for LDA...")
    tf_vectorizer = CountVectorizer(min_df=1,stop_words='english')
    tf = tf_vectorizer.fit_transform(final_corpus)
    n_samples = tf.shape[0]
    n_features = tf.shape[-1]
    print (n_features)
    print("Fitting LDA models with tf features n_samples=%d and n_features=%d..."
              % (n_samples, n_features))

#Applying LDA and extracting topics
    lda = LatentDirichletAllocation(n_topics=n_topics, max_iter=5,
                                    learning_method='online',
                                    learning_offset=50.,
                                    random_state=0)
    lda.fit(tf)
    print("\nTopics in LDA model:")
    tf_feature_names = tf_vectorizer.get_feature_names()
    print_top_words(lda, tf_feature_names, n_top_words)
    #return tf_feature_names

topic_model(read_corpus())