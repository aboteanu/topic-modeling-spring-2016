                                    20130618 6

                    [0:00] [background conversation]

  David:            [0:01] ...squeeze [inaudible 00:01] together and tap on the screen
                    to interact, and if gets tired, [inaudible 00:05] here and let me
                    know [inaudible 00:06] .

  Woman:            [0:09] OK. OK, buddy, you know what you need to do. Wanna tap the
                    yellow duck? Tap, tap.

  Child:            [inaudible 00:17] [0:17] .

  Woman:            [0:22] Hello, hello? Is anyone here? Tap me, show me someone is
                    there. Is someone there? Help me, help me. Help me be free. Tell
                    me. Tell me, what do you see?

  Child:            [inaudible 00:38] [0:39] .

  Woman:            [0:40] I'm a baby duck! What color can I be?

  Child:            [0:44] Red!

  Woman:            [0:48] Go to the tub. Rub, dub, dub. Red, yellow, blue, what color
                    are you? What color?

  Child:            [0:57] Red.

  Woman:            [0:58] Do you wanna touch the red paint, then? See what it does.
                    Red. Try touching the yellow paint and see what it does. It makes
                    orange. Red and yellow makes orange. What color did it make?

  Child:            [1:17] Brown.

  Woman:            [1:18] Brown, that's right. Look at me, I am brown. Did you make
                    the duck brown?

  Child:            [1:30] Purple.

  Woman:            [1:31] Purple, purple mirror. Yup. Nope, not that arrow, unless you
                    want to make him another color. Do you want to make him another
                    color? OK. What color? What color did you make him?

  Child:            [1:48] White.

  Woman:            [1:48] White. That's right! Well, the water's blue. Yeah. It washes
                    all the paint off and makes him white. Do you want to make him a
                    different color?

  Child:            [2:01] Red.

  Woman:            [2:05] Brown.

  Child:            [2:06] Yellow.

  Woman:            [2:07] Can we go see what's next? Hey, look at me. I am orange. I
                    am an orange duck. Where can I play? Go the purple frog. He will
                    find the way. Where's the purple frog? Where's the purple frog?
                    Purple frog, take me to play. Go to the white frog. He will find
                    the way. Where's the white frog?

                    [2:45] White frog, take me to play. Back. Go to the purple frog. He
                    will find the way. Purple frog, take me to play! And, then, let's
                    see what's next. Go to the red frog. He will find the way. Red
                    frog, take me to play. I want to play. What can ducks do? Let's do
                    this one. The white frog. He will find the way. White frog, take me
                    to play. I want to play. What can ducks do? What do ducks do,
                    Bennet? Don't, don't hit that one. You're not, you're going to go
                    back. What can ducks do?

  Child:            [3:37] Can I do this?

  Woman:            [3:39] Do you want to do a different one?

  Child:            [3:41] This, Mama.

  Woman:            [3:42] Oh, you want to paint?

  Child:            [3:43] Yeah.

  Woman:            [3:46] Let's see what that does. Swim, splash, dive, run, Baby Dee
                    can go have fun.

  Child:            [3:53] Paint.

  David:            [3:58] OK.

  Woman:            [3:59] We have an iPad. So, I think [inaudible 04:01] . I am done.
                    That was fun! Is the duck tired? I am hungry. What do ducks eat? Do
                    you know what ducks eat?

  Child:            [4:13] Food.

  Woman:            [4:14] Food. What kind of food? Let's see. Ducks eat bugs. One bug,
                    two bugs, three bugs, four. Eat six bugs. Do you want more?

  Child:            [4:28] No.

  Woman:            [4:28] Do you know ducks eat bugs? One. See the number one. Two.
                    Why don't you bring it up to the duck. Drag it. Drag it to the
                    duck. Good. Good. Do it again. You almost got it. Oh, you did it!
                    Look at him eating, eat the ladybug. Do you want to give him
                    another bug? Tap more.

                    [5:23] [crosstalk]

  Child:            [5:26] Sleeping.

                    [5:29] [crosstalk]

  Woman:            [5:40] What if we put another one [inaudible 05:40] outside?

                    [5:42] [crosstalk]

  David:            [laughs] Right. [inaudible 06:22] [6:21]

                    [6:23] [crosstalk]

  Woman:            [6:24] "I want more," says the duck. OK, Baby Dee. You can eat two
                    bugs. [inaudible 06:36] two bugs.

  Child:            [inaudible 06:41] [6:41] .

  Woman:            [6:42] Mm, [inaudible 06:43] . What do you say? My eyes feel funny,
                    why is that?

  Child:            [inaudible 06:49] [6:49] .

  Woman:            [6:57] Are you tired Baby Dee? Let's tuck you in bed and lay down
                    your head. Is it nap time? You wanna take those leaves, and put it
                    on top and [inaudible 07:11] .

                    [7:12] [crosstalk]

  Woman:            [7:20] I'm a duck. I can play and run all day. I eat bugs to
                    [inaudible 07:26] . Just one thing more! [inaudible 07:31] song.
                    [inaudible 07:34] .

                    [7:35] [crosstalk]

  Woman:            [inaudible 07:55] Baby Dee [inaudible 07:56] [7:53] . Is that fun?
                    Did you like that story?

  Child:            [7:59] Yeah.

  Woman:            [8:00] You did? [inaudible 08:00] .

  David:            [8:00] So, how old is he?

  Woman:            [8:04] He's two and three months.

  David:            [8:06] Do you usually read to him?

  Woman:            [8:10] Ah, normally about two hours a day so... [ inaudible 08:17]
                    hours.

  David:            [8:16] Yeah, yeah. [inaudible 08:19] .

  Woman:            [inaudible 08:35] a few questions, then we can go play [inaudible
                    08:37] [8:35] . You wanna do that?

  Child:            [8:39] Yeah.

  Woman:            [8:40] OK.

  David:            [8:40] Do you have any suggestions [inaudible 08:41] .

  Woman:            [8:44] Um, yeah, being able to go back.

  David:            [inaudible 08:54] [8:54] .

  Woman:            [inaudible 08:58] [8:58] , and then...I don't know. I mean, it
                    wasn't that great of a story.

  David:            [9:07] Yeah.

  Woman:            [9:08] It didn't have a story line.

  David:            [9:09] Yeah.

  Woman:            [9:09] Kind of a [inaudible 09:11] .

  David:            [9:15] So, what where doing is we're, we're, we believe that
                    [inaudible 09:20] parents read together. So, we're trying to
                    compare technology to [inaudible 09:30] see if we can provide
                    [inaudible 09:32] . You did a great job. [inaudible 09:40] . Do you
                    have any questions [inaudible 09:44] ?

  Woman:            [9:49] Is this, are you a child [inaudible 09:54] school or?

  David:            [9:56] No, actually. No, I'm primarily focused on [inaudible 10:01]
                    .

  Woman:            [10:02] OK.

  David:            [10:02] One of our [inaudible 10:03] when listening [inaudible
                    10:07] .

                    [10:08] [crosstalk]

  Woman:            [10:09] OK.

  David:            [inaudible 10:12] playing with, and see if you can [inaudible
                    10:19] [10:10] .

  Woman:            [10:20] OK.

  David:            [10:21] So, you're working [inaudible 10:29] .

  Woman:            [10:28] OK, um, maybe one thing [inaudible 10:31] story is
                    [inaudible 10:32] like where he's [inaudible 10:34] . Baby Dee
                    wants to go to bed [inaudible 10:39] .

  David:            [10:39] Sure. Did you see [inaudible 10:41] ...

  Woman:            [10:42] Yeah.

  David:            [10:43] ... [inaudible 10:44] wasn't enough.

  Woman:            [10:44] Um, I'd almost want [inaudible 10:47] like for that I want
                    him to be able...

  David:            [inaudible 10:50] [10:49] .

  Woman:            [10:51] ...saying things rather than [inaudible 10:54] um, visual,
                    [inaudible10:57] . Did you like the story? [inaudible 10:56] .

  David:            [11:02] Do you want a sticker? Can I give you a sticker?

  Woman:            [11:04] Yeah. Do you want a sticker baby? What do you say?

                    [11:07] [crosstalk]

  Woman:            [11:15] All right, come here. What do you say?

  David:            [11:21] If you want a card, Jackie has [inaudible 11:23] .

  Woman:            [11:23] Oh, OK.

  David:            [11:24] So, thank you. [inaudible 11:27] You sure? Cause that was
                    different. [inaudible 11:29]

                    [11:30] [crosstalk]

  Woman:            [11:30] Do you guys care, if, um, teachers are...

  David:            [11:32] Yeah, as a matter of fact we have. We're working right now
                    [inaudible 11:36] to provide us a lot of feedback [inaudible 11:42]
                    .

  Woman:            [11:42] Cool!

  David:            [11:43] Yeah.

  Woman:            [11:45] MIT doesn't have a [inaudible 11:47] .

  David:            [11:47] Ah, no [inaudible 11:48] .

                    [11:48] [crosstalk]

  David:            [inaudible 11:54] [11:49] .

  Woman:            [inaudible 11:55] [11:54] might be a big jump...



Transcription by CastingWords
 