                                    20130618 4

                    [background conversations]

  Woman:            [0:03] ...Baby D!

  Ethan:            [0:04] I [inaudible 00:04] .

  Woman:            [0:07] I'm a baby duck. I am Baby D! What is she trying?

  Tablet:           [0:13] Baby D!

  Woman:            [0:15] Do it again.

  Tablet:           [0:17] Baby D!

  Woman:            [0:19] What does it say?

  Ethan:            [inaudible 00:21] [0:20] flag.

  Woman:            [0:32] I'm a baby duck.

  Tablet:           [0:35] Duck! Duck! Duck!

  Woman:            [0:40] Where is he?

  Tablet:           [0:41] Duck!

  Woman:            [0:46] Where is the duck? What color is this? Is this brown? Look.
                    Go to the tub. Rub, rub, rub. Red, yellow, blue. What color are
                    you? The red arrow. Look at me. I am white. Let's see what happens.
                    Is he a white duck? Look, Ethan. This one's available. Look at this
                    one. I'm a white duck. Where can I play? Ethan, look. Can you push
                    the red arrow? Come push it for me. Push it for me.

  Ethan:            [1:35] Here.

  Woman:            [1:36] Yeah, that one's not ours. This one's available. Can you
                    push the red arrow? Push the red arrow. Good. Go to the white frog,
                    he will find the way. Push the white frog. Push the white frog.
                    There he is. White frog, take me to play. Good.

                    [2:05] Want to push the red. I want to play. What can ducks do?
                    Push the red arrow. Swim, splash, dive, run, Baby D, go have fun!
                    Wow. Push the red arrow. Try again. Good. I am hungry. What do
                    ducks eat? What do they eat? What do we feed a duck? Do we feed the
                    ducks bread? Push the right arrow.

                    [2:54] I want more! He's hungry. See, his mouth is open. I want
                    more. OK, baby D. You can eat 10 bugs. We will see. Look at those
                    bugs. Are they ladybugs? We have ladybugs? 1, 2, 3, 4, 5, 6, 7, 8,
                    9, 10, 11, 12, 13, 14, 15, 16, 17! Oh! She's eating the bugs.
                    Chomp. Chomp. Chomp.

                    [3:31] Oh, excuse me, duck. My eyes feel funny. Why is that? Why do
                    you think her eyes feel funny? Is she getting sleepy? You are
                    tired, baby D. It was your first big day. Let's tuck you in bed.
                    Lay down your head.

                    [3:56] What do you think that is, Ethan? What's that yellow thing?
                    Is that the duck's blankey? Push the red arrow. Good job! I am a
                    duck, a white duck. I can play and run all day. I eat bugs, two,
                    three, four. I sleep, just one thing more. OK, Ethan. Let's look at
                    that. Push the red arrow. You are doing an excellent job.

                    [4:27] Look here. OK, baby D. We'll sing you a song. Bird, bee, and
                    frog will all sing along. Ethan, where's the frog? Where's the
                    frog? Ribbit. Ribbit. Do you see a frog? I see a frog. There's the
                    frog!

  Tablet:           [4:50] Frog, frog.

  Woman:            [4:51] Do you see a bird? Where's the bird? Yes, you're right.
                    Where's the bumble bee. Yes, yes! Good.

  Ethan:            [5:07] I see a bumble bee.

  Woman:            [5:09] Go to sleep, baby D. Not a peep, Baby D. Lay down until a
                    new day comes, all done. We're finished, all done.

  Woman:            [5:28] Did you like that story?

  Interviewer:      [5:33] What did you like best about it?

  Woman:            [5:38] That story.

  Interviewer:      [5:39] How old is he.

  Woman:            [5:40] He's two. He'll almost be two and a half.

  Interviewer:      [5:42] He has a lot to say. That's exciting.

  Woman:            [5:45] Ethan! Hey, Ethan!

  Interviewer:      [5:49] That's all right. That's OK. Basically, we just wanted to
                    mention that [inaudible 05:54] kids read together, specifically on
                    tablets and other digital devices, because a lot of people use them
                    now.

  Woman:            [6:02] Yeah. We don't do it very much.

  Interviewer:      [6:09] Well, it's a great way to read.

  Woman:            [6:11] Yes.



Transcription by CastingWords
 