                                    20130730 4

                    [0:00] [background conversation 0:00:00-0:00:22]

  David:            [0:22] How old are you?

  David:            [inaudible 0:00:24] honest. [laughs] [0:25]

                    [0:26] [background conversation 0:00:28-0:01:00]

  David:            [1:00] That's fair. Thank you so much. So, I'm going to ask you to
                    read a story together, on this tablet. You can touch the pictures.
                    You can play with the things you see, and, if you want to stop,
                    just stop. All right? And feel free to talk about things.

                    [1:17] [inaudible background conversation 0:01:28-0:01:31]

  Parent:           [1:31] To see for him to figure out...

  Parent:           [1:32] What do you see? Do you a see a chicken? A duck? It's a baby
                    duck. My name is Baby D.

  Child:            [1:49] Baby D. Baby D. Baby D. Baby duck. Baby D. Baby D.

  Parent:           [1:52] Want to turn the page. Want to? Turn the page. What is he
                    doing?

  Child:            [2:22] Baby D.

  Parent:           [2:22] Do you like taking a bath?

                    [2:24] [background conversation]

  David:            [2:25] ...and that's just where [inaudible 0:02:39] .

  Parent:           [2:28] Do you like [inaudible 0:02:44] .

                    [2:29] [crosstalk]

  Child:            [2:34] OK, [inaudible 0:02:48]

  Parent:           [2:50] You could get the [inaudible 0:02:59] bubble.

  Parent:           [2:58] Do you want to [inaudible 0:03:03] . Where is the blue?

  David:            [3:26] Yeah, yeah, yeah.

                    [3:28] [background conversation 0:03:27-0:06:31]

  Parent:           [6:31] Oh, he is so hungry.

                    [6:32] [background conversation 0:06:38-0:08:00]

  David:            [8:01] Thank you. Can I give her a sticker?

  Parent:           [8:11] Yeah, sure. Do you want a sticker?

  David:            [8:19] So, we're studying how people use things -- books like this,
                    versus real books, or physical books.

  Parent:           [8:58] And, and do you think, think that the iPad is [inaudible
                    0:08:59] is going to be available to...that's the question I have.

  David:            [laughs] [9:03] It's a big question, and we don't know the answer.
                    That's why we're trying to study. So, what we're doing is, we're
                    listening to the words...

  Parent:           [9:11] Wow.

  David:            [9:11] ...that you use...so you ask her about a question...

  Parent:           [9:13] Wow. Wow.

  David:            [9:14] "What do you think this was?" or "What do you...what happens
                    when you mix these colors?" We want to see if we can design a book
                    that...

                    [9:20] For cases where parents can't read to their kids, or where
                    parents are...then maybe the book can help them ask these kinds of
                    questions also.

  Parent:           [9:28] Amazing. I have to say, uh, I wasn't going to because of
                    like, the iPad.

  David:            [9:33] Yeah.

  Parent:           [9:34] My sister gave it to her for her nap.

  David:            [laughs] [9:37] .

  Parent:           [9:39] I don't have an iPad.

  David:            [9:41] But she does. It's, it's amazing...

  Parent:           [9:44] How the kids, they know so much.

  David:            [9:46] I was watching when you said you were going to...She knows
                    that it's...

  Parent:           [9:50] That's how the iPhone works.

  David:            [9:51] So, they know. They can... [laughs]

                    [9:53] It's good to know, so...on their way, so we just want to
                    make sure to make some games that are really educational, that
                    are...

                    [10:02] Thank you so much kids. Did you get one of these papers, to
                    show you what...?

  Parent:           [10:07] Oh, I did.

  David:            [10:08] Oh, you got one. OK, very good.

  Parent:           [10:09] Thank you so much.

  David:            [10:10] Bye, Sophia. Good to meet you.

  Parent:           [10:12] Thank you.

  Parent:           [10:12] Thank you.

  David:            [10:14] Thank you so much. Bye, Sofia. Good to meet you. Bye.

  Parent:           [10:16] Bye.

  David:            [10:17] Oh, hey!

  Parent:           [10:17] Hello!

  David:            [10:18] Hey...

  Parent:           [inaudible 0:10:17] [10:19] story.

  David:            [10:20] Great! Are you [inaudible 0:10:19] ?

  Parent:           [10:21] Yes.

  David:            [10:22] Do you mind if I ask you [inaudible 0:10:21] ?

  Parent:           [inaudible 0:10:22] [10:23]

  David:            [10:24] Great. So...



Transcription by CastingWords
 