Male:	Um, well, I don't know that we have [inaudible 00:00:08]. Um, we don't [inaudible 00:00:12].
Female:	Tell me, tell me, what do you see?
Female:	A baby duck.
Female:	Baby duck. That's her name?
Female:	Yeah.
Female:	Is it a boy or girl?
Female:	Boy.
Female:	A boy duck, okay. 
Female:	[Inaudible 00:00:28].
Female:	I see. 
Male:	I got you, yeah. 
Male:	Is there a [inaudible 00:00:51] buzz. 
Female:	Oh. Did you touch ...
Female:	All right.
Female:	All right, do you want to add another one to that?
Female:	What color?
Female:	Pink. White.
Female:	White? Do you want [inaudible 00:01:12]?
Female:	No. No. 
Female:	What are these for?
Female:	What does 'D' stand for? 
Male:	I think one of the main problems with that [inaudible 00:01:30].
Female:	You want another one? [Inaudible 00:01:34] do you want it.
Female:	For me. 
Female:	[Inaudible 00:01:37] even longer?
Female:	[Inaudible 00:01:40].
Female:	Wow.
Female:	Wow.
Female:	Ooooh.
Female:	White. 
Female:	White. Pick the color blue. 
Female:	Do you want it [inaudible 00:01:58]? [02:00].
Female:	(Laughs). 
Female:	Those are good. Here. 
Male:	Here, give me those [inaudible 00:02:04]. 
Female:	Yeah, [inaudible 00:02:10].
Female:	White.
Female:	[Inaudible 00:02:15].
Female:	There you go. Fun. Whoe.
Male:	Cool.
Female:	Whoe. Daddy go.
Female:	Talk.
Female:	What's the language? 
Female:	[Inaudible 00:02:34].
Female:	Where'd they go?
Female:	Hi.
Male:	A lot more ...
Female:	Hello frog. Frog. Hello frog. Pink. To play. 
Male:	The question is ...
Male:	Yeah, yeah, yeah ...
Female:	Pink. 
Female:	There's thing, a little frog.
Female:	Frog. Duck.
Male:	Still has ...
Female:	Bye frog. 
Female:	Wait, wait [inaudible 00:03:45].
Female:	Pink.
Female:	Bye. Bubye. [04:00].
Female:	That's fun.
Male:	You guys a ... You're [inaudible 00:04:09].
Female:	Whooo.
Female:	He said those ...
Female:	Oh, I don't want [inaudible 00:04:38]. 
Female:	(Laughs). 
Female:	What a big, fat frog. [Inaudible 00:04:42]?
Male:	Employed already ...
Female:	Four. 
Male:	[Inaudible 00:05:02]. Part of this project outline, if we're still ...
Female:	I think that's something that we've made before. Yeah.
	Oh, take it he wants more.
Male:	Thank you, very much.
Female:	Are going to give him more?
Male:	Yeah, he can just sit over here.
Female:	Some more?
Female:	Look who's here.
Male:	Yeah. 
Female:	(Yawn). [06:00].
Female:	Hi. Why? Why? Hi. (Yawn).
Female:	(Laughs). 
Male:	Thank you so much, going to give you a sticker. You don't want sticker?
Female:	What do you say?
Female:	Thank you.
Male:	You're welcome. So, we're just watching the the words that he's using ...
Female:	Mm hmm. (Affirmative). 
Male:	Words that they use when they talk to each other. 
Male:	And it was very interesting, because most kids don't realize that they can touch the words [inaudible 00:06:49]. It was funny watching him do everyone of the words. It was really exciting to see that. [Inaudible 00:06:54]. 
Male:	All right. (Laughs). 
Male:	You have any questions about anything?
Female:	Um, yeah, what are you [inaudible 00:07:00]?
Male:	So, right. We're we're hoping to [inaudible 00:07:02] kind of behaviors that you're exhibiting. And use that to [inaudible 00:07:05]. So, a lot of parents say they don't [inaudible 00:07:11] read with their kids. They they they they can't because they have all these [inaudible 00:07:16]. If there's a way to [inaudible 00:07:19] help them out by giving them [inaudible 00:07:21]. 
Female:	Okay.
Male:	[Inaudible 00:07:28] getting kids to experience language [inaudible 00:07:31]. It also [inaudible 00:07:32].
Female:	Did you like that story? Yeah? How old are you?
Female:	Three.
Female:	You're talking. (Laughs). 
Male:	Sticker problems. (Laughs). 
Female:	You got a sticker in your hair. Better than a gum in your hair, huh?
Female:	Okay. You want [inaudible 00:07:48]? Put it on your shirt. There you go. Say ... Say bye.
Male:	Bye, thank you so much for your time, we appreciate it. 
Female:	(Laughs). 
Male:	Well, isn't that fun. 
Female:	(Laughs). 
Male:	Thank you again.
Female:	You're welcome. [08:00]. Have a good day. 
Male:	Did more than I thought.
Female:	Oh, thank you. Henry, wait for me.
Male:	That was interesting, wow. We ended up talking about, a lot about this research. And then also, some other [inaudible 00:08:20]. That guy was a researcher in [inaudible 00:08:23] the CDC. 
Male:	Oh, wow. (Laughs). 
Male:	Yeah. He was talking about how, the Ethiopian stuff [inaudible 00:08:29] as the [inaudible 00:08:31] platform of her, like public health.
Male:	That's exactly what um, what Tim's in ... So, not really [inaudible 00:08:38].
Male:	Oh.
Male:	Friend of Tim [Z 00:08:41].
Male:	Oh weird. 
Male:	Um, I don't know, that'd [inaudible 00:08:45].
Male:	Tim, Tim [inaudible 00:08:46].
Male:	(Laughs). So, Tim then, is like the personal ... Like, personal advisor to [inaudible 00:08:55]. But, because that ... (Laughs). So, but anyway, this guy was saying that like we can be looking at [inaudible 00:09:04] as as just um literacy. [Inaudible 00:09:11]. Social literacy, health literacy. It's just to educate the Bible. That's that's actually where like, [inaudible 00:09:21]. 
Male:	It's either like, like that's probably, I'm guessing he's not a scientist.
Male:	(Laughs). I doubt it.
Male:	No. Maybe [inaudible 00:09:30] addition. That that seems like what the parents like [inaudible 00:09:34] these goals versus [inaudible 00:09:37]. You know [inaudible 00:09:41] do we do the most good now or do we actually, like examine how to do what's good in the future?
Male:	Ian, I think reality is [inaudible 00:09:53]. We just got to [inaudible 00:09:55] hire the best. You know, [Inaudible 00:09:58]. 
Male:	Yeah. [10:00].
Male:	I mean, [inaudible 00:10:01] these guys will go.
Male:	Right.
Male:	What they're trying to do. Yeah. But it also makes [inaudible 00:10:07] the line of work that I've been doing. How do you, how do you scale [inaudible 00:10:12]? In reality, and specially since [inaudible 00:10:14].
Male:	Makes sense. Like I should be thinking more about [inaudible 00:10:17].
Male:	Okay. [Inaudible 00:10:18]. Yeah. Well, you know, [inaudible 00:10:20]. Today, what is this? What is the question? 
Male:	Yeah. [Inaudible 00:10:27].
Male:	(Laughs). 
Male:	Yeah, right.
Male:	It is kind of a overall goal. We want to learn how. Yeah. Kid learn, yeah and we want to ... 
Male:	I think, I think that's one of my like, bigger reservations. Scientific number being arranged. But vague, [inaudible 00:10:54] truly in order, so like vague on hypothesis.
Male:	Very true. Very true.
Female:	(Laughs).
Male:	And both of us are are, have inherited this particular project. But then I'm much more super [inaudible 00:11:07]. I think, they think that we're talking about like, [canal 00:11:13], like leverage through some [inaudible 00:11:14] like a normal soul. 
Male:	Yeah. 
Male:	I'll become aware [inaudible 00:11:20].
Male:	Yeah, it's interesting, and then the end, it's really not [inaudible 00:11:22].
Male:	(Laughs). I mean, if I can, you know. If I could blind [inaudible 00:11:31]. 
Male:	Yeah. Well ah, would you be [inaudible 00:11:38] switching? [Inaudible 00:11:39].
Male:	Um, yeah, definitely.
Male:	We have a couple these [inaudible 00:11:43] left. I think I've handed out probably five, six. [Inaudible 00:11:48]. Educational opportunities.
Male:	[Inaudible 00:11:53].
Male:	Oh, the mic's on.
Male:	[Inaudible 00:11:57]. Yeah.
Male:	Very becoming.
Male:	Especially, yeah. [12:00]. (Laughs). 
Male:	(Laughs). 
Male:	Are they going to be listening to this? (Laughs). 
Male:	Yeah, I'm going to do that anyway, because I don't know and it ... Like, I want to cut out every like ...
Male:	[Inaudible 00:12:10] need to give to the parents [inaudible 00:12:12].
Male:	[Inaudible 00:12:13]. So, right, one more consent form, right? That's all you have?
Male:	Do you have the recording?
Male:	Mm hmm. (Affirmative).
	Off here too. Nope. 
Female:	Here. 

