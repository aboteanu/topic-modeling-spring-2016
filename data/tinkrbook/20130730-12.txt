Male:	Ah, well, I don't know that we have ... Based on we have a lot of [inaudible 00:00:08] volume. Um, we don't have [inaudible 00:00:12].
Female:	Tell me, tell me, what do you see?
Female:	A baby duck.
Female:	Baby duck. That's her name?
Female:	Yeah.
Female:	Is it a boy or girl? 
Female:	Boy.
Female:	A boy duck, okay.
Female:	You see, it's a boy.
Female:	I see. You want to push that in [inaudible 00:00:31]?
Female:	We'll talk.
Female:	A. B. C.  A. B. C.
Male:	I got you, yeah.
Male:	Do a lot of work ...
Female:	Wow.
Male:	Buzz about ... 
Female:	Touch this. 
Female:	[Inaudible 00:00:57].
Female:	White. 
Female:	White.
Female:	What. What color is bee? 
Female:	White.
Female:	White. Do you want to go [inaudible 00:01:12]?
Female:	No. 
Female:	[Inaudible 00:01:15].
Female:	What's 'D' for? 
Female:	Baby [inaudible 00:01:23].
Male:	(Laughs). 
Female:	What's 'D' stand for? 
Male:	But ah, I think one of the main problems with that ...
Female:	Out.
Female:	You want another one? 
Female:	Out.
Female:	What one do you want it?
Female:	Out. For me.
Female:	[Inaudible 00:01:38] longer? 
Female:	What. Pink. 
Female:	Wow. 
Female:	[Inaudible 00:01:46] like that.
Female:	Oooh.
Female:	White.
Female:	White. Pick the color blue.
Female:	[Inaudible 00:01:59]? [02:00].
Female:	(Laughs). 
Female:	Those are good. Here. 
Female:	Here, give me those [inaudible 00:02:09].
Female:	Yeah, I think [inaudible 00:02:10].
Female:	White.
Female:	White again. 
Male:	Working phases into [inaudible 00:02:20].
Male:	Uh huh. (Affirmative). 
Female:	There you go. Fun.
Female:	Crawl. 
Female:	Whoe. [Inaudible 00:02:28].
Female:	Talk. 
Female:	What's the language [inaudible 00:02:33]? 
	Where'd they go?
Female:	Talk. Cake. Hi. Two. Talk. Hello. Dog. Frog. Hello. Frog. Pink. Two. Hi. Pink. 
Female:	There [inaudible 00:03:24] a little frog.
Female:	Frog. Two. Frog. Frog. Jump. 
Female:	Bye bye frog.
Female:	Bye frog.
Female:	Hey, wait, wait, no. 
Female:	Pink. 
Female:	White. [04:00].
Female:	That's fun. 
Male:	So you guys, ah, you're [inaudible 00:04:09]. For the average part [inaudible 00:04:17].
Female:	There's some right [inaudible 00:04:22].
Female:	(Laughs). 
Female:	Look a big fat frog.
Female:	[Inaudible 00:04:42].
Female:	Four. Five. 
Female:	Think that's something that we've made before. 
Male:	Oh.
Female:	Oh, take it he wants more.
Male:	Thank you, very much.
Female:	You going to give him more?
Male:	Yeah, he can just sit over here. 
Female:	You are?
	If he gets tired ...
Male:	Yeah.
Female:	(Yawn). [06:00]. 
	Why I feel tired? Why? Why? Is that? [Inaudible 00:06:20].
	(Yawn).
Female:	(Laughs). 
Male:	Thank you so much. Can I give you a sticker? You don't want a sticker?
Female:	Oh, what do you say?
Female:	Thank you.
Male:	You're welcome. So, we're just watching the the words that he uses. 
Female:	Mm hmm. (Affirmative).
Male:	Words they use when they talk to each other.
Male:	And it was very interesting, because most kids don't realize that they could touch the words [inaudible 00:06:49]. It was funny watching him do every one of the words.
Female:	Yeah.
Male:	It was really exciting to see that. 
Female:	All right.
Male:	Do you have any questions about anything?
Female:	Um, yeah, what are you [inaudible 00:06:59]?
Male:	So, right. We're we're hoping to [inaudible 00:07:02] kind of behaviors that you're exhibiting. And use that to [inaudible 00:07:05]. So, a lot of parents say they don't [inaudible 00:07:11] read with their kids. They they they they can't because they have all these [inaudible 00:07:16]. If there's a way to [inaudible 00:07:19] help them out by giving them [inaudible 00:07:21]. 
Female:	Okay.
Male:	[Inaudible 00:07:28] getting kids to experience language [inaudible 00:07:31]. It also [inaudible 00:07:32].
Female:	Did you like that story? Yeah? How old are you?
Male:	Three.
Female:	Wow, you're really good for [inaudible 00:07:38].
Female:	(Laughs). 
Male:	You had a sticker problem. (Laughs). 
Female:	You got a sticker in your hair. Better than having a gum in your hair, huh? 
Female:	Okay. You want [inaudible 00:07:48]? Put it on your shirt. There you go. Say ... Say bye.
Male:	Bye, thank you so much for your time, we appreciate it. 
Female:	(Laughs). 
Male:	Well, isn't that fun [inaudible 00:07:58]. Thank you again.
Female:	You're welcome. [08:00]. Have a good day. 
Male:	Did more than I thought. 
Female:	Oh, thank you, Henry, wait for me. 
Male:	That was interesting. We ended up talking about, a lot about this research. And then also, some other things. That guy was a researcher in C the CDC. 
Male:	Oh, wow. (Laughs). 
Male:	Yeah. He was talking about how, the Ethiopian stuff. In particularly, as the [inaudible 00:08:31] platform of her, like public health.
Male:	That's exactly what um, what Tim's in ... So, not really Tim Z. But they did the [inaudible 00:08:40]. Tim Z.
Male:	Oh.
Male:	A friend of Tim Z.
Male:	Oh weird.
Male:	Um, I don't know [crosstalk 00:08:44].
Male:	Dog, Rin Tin Tin. Yeah.
Male:	(Laughs). So, Tim then, is like the personal ... Like, personal advisor to the Dahli Llama. [Inaudible 00:08:56]. But, because that ... (Laughs). So, but anyway, this guy was saying that like we can be looking at this common stuff as as just um literacy. What do we mean? Well literacy, but literacy in a much broader sense. Like, social literacy, health literacy. It's just to educate the Bible. That's that's actually where like, [inaudible 00:09:21]. 
Female:	Whoo!
Male:	It's either like, like that's probably, I'm guessing he's not a scientist.
Male:	(Laughs). I doubt it. 
Male:	No. Maybe [inaudible 00:09:30] addition. That that seems like what the parents like [inaudible 00:09:34] these goals versus [inaudible 00:09:37]. You know [inaudible 00:09:41] do we do the most good now or do we actually, like examine how to do what's good in the future?
Male:	Ian, I think reality is [inaudible 00:09:53]. We just got to hire the best. You know, [Inaudible 00:09:58]. 
Male:	Yeah. [10:00].
Male:	I mean, [inaudible 00:10:01] these guys will go.
Male:	Right.
Male:	What they're trying to do. Yeah. But it also makes [inaudible 00:10:07] the line of work that I've been doing. How do you, how do you scale this in the real world? In reality, and specially since [inaudible 00:10:14].
Male:	Makes sense. Like I should be thinking more about [inaudible 00:10:17].
Male:	Okay. [Inaudible 00:10:18]. Yeah. Well, you know, [inaudible 00:10:20]. Today, what is this? What is the question? 
Male:	Yeah. [Inaudible 00:10:27].
Male:	(Laughs).
Male:	Yeah, right.
Male:	It is kind of a overall goal. We want to learn how. Yeah. Kids learn, yeah and we want to ... 
Male:	I think, I think that's one of my like, bigger reservations. Scientific numbers being arranged. But vague, [inaudible 00:10:54] truly in order, so like vague on hypothesis.
Male:	Very true. Very true. 
Male:	And both of us are are, have inherited this particular project. But then I'm much more super excited about having to think, to think that we're talking about like, [canal 00:11:13], like leverage through some [inaudible 00:11:14] like a normal soul. 
Male:	Yeah. 
Male:	I'll become aware [inaudible 00:11:20].
Male:	Yeah, it's interesting, and then the end, it's really not structured [inaudible 00:11:22].
Male:	(Laughs). I mean, if I can, you know. If I could blind [inaudible 00:11:31]. 
Male:	Yeah. Well ah, would you be interested in switching? We've got about an hour [Inaudible 00:11:39].
Male:	Um, yeah, definitely.
Male:	We have a couple these [inaudible 00:11:43] left. I think I've handed out probably five, six. Just need to mark those. Oh, Educational opportunities.
Male:	It's a good idea ...
Male:	Oh, the mic's on.
Male:	[Inaudible 00:11:57]. Yeah.
Male:	That's very becoming.
Male:	Especially, yeah. [12:00]. (Laughs). 
Male:	Are they going to be listening to this? (Laughs). 
Male:	Yeah, I'm going to do that anyway, because I don't know and it ... Like, I want to cut out every like ...
Male:	[Inaudible 00:12:10] need to give to the parents [inaudible 00:12:12].
Male:	[Inaudible 00:12:13]. So, right, one more consent form, right? That's all you have?
Male:	Do we have the recording off here too? Nope. 
Female:	Here. 

