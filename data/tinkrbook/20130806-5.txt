                                    20130806 5



  David:            [0:02] Are you ready?

                    [0:03] [crosstalk]

  David:            [0:04] So, I'm going to have you read the story. [inaudible 0:06]
                    Have you ever done this before?

                    [0:08] [crosstalk]

  Child:            [0:10] I have [inaudible 0:10] like that.

  David:            [0:11] Nice, now you can tap on the screen and type it out.

                    [0:12] [crosstalk]

  Woman 1:          [0:13] Oh, thank you, [inaudible 0:12] 24 hours [inaudible 0:14] .

                    [0:15] [crosstalk]

  Woman 2:          [0:16] Zachary [inaudible 0:16]

  David:            [crosstalk] awesome [inaudible 0:23] ...put this together and
                    [inaudible 0:25] [0:18]

                    [0:27] [child crying]

  Tablet:           [0:33] Tap, Tap, Tap

                    [0:37] [crosstalk]

  Woman 3:          [0:39] Oh, cool!

                    [0:43] [crosstalk]

  Woman 4:          [0:47] Hello. Hello.

                    [0:50] [crosstalk]

  Woman 2:          [0:54] Yeah, we're going to get [inaudible 0:54] and paper

  Woman 4:          [0:56] Is anyone here?

                    [1:00] [crosstalk]

  Female:           [1:02] Hello. Hello.

  Child:            [1:04] Hello.

  Female:           [1:05] Is anyone here? Tap.

  Tablet:           [1:07] Show.

  Female:           [1:11] Show me someone is there. Tap. Again. The. Sound it out.
                    Perfect. Let me free. Sound that word. You can do it. You can do
                    it. Tap. Tell me.

  Child:            [2:20] Tell me.

  Female:           [2:21] What do you...

                    [2:22] [crosstalk]

  Child:            [2:24] See.

  Female:           [2:25] See.

  Tablet:           [2:26] See. See.

  Female:           [2:32] You are a duck. You are a...sound it out. Sound it out.

                    [2:55] [classroom chatter]

  Woman 5:          [3:02] I am a baby duck. I am Baby...

  Child:            [3:05] D.

  Woman 5:          [3:08] I am a baby duck. I [inaudible 3:14] to the...What's that
                    sound?

  Child:            [3:27] "Ra."

  Woman 5:          [3:29] That's right. ra. What is it? No. This word. What is this
                    word? What is it ra. Two letters sound it out.

                    [3:50] Ra ra ra. I [inaudible 3:59] . I am a.

                    [4:03] [classroom chatter]

  Woman 5:          [4:15] Where can I? Where can I what? [inaudible 4:30] . Where can
                    I [inaudible 4:36] ? Sound it out.0

                    [4:48] [electric drill noises] .

  Woman 5:          [inaudible 5:03] That's a site word. [inaudible 5:16] [5:03]

                    [5:21] [crosstalk and background chatter]

  David:            [5:37] Thank you so much for your time. Want a sticker? So, what
                    we're doing is...watching how [inaudible 5:52] . Yeah, they're both
                    for you.

  Child:            [5:56] Mom, you want to.

                    [5:58] [crosstalk and classroom chatter]

  David:            [5:59] So things like watching how they tap along and ask
                    questions. So do you want to sit here. [inaudible 6:10] on the
                    paper here. We can try. Do you have any questions on the research.



Transcription by CastingWords
 