Speaker 1:	Thank you so much.  [inaudible 00:00:02].
David:	What's your name again?
Child:	Um, [Mia 00:00:06].  
David:	Mia.  It's good to meet you.  My name's David.  I', a researcher from MIT.
Speaker 1:	I'm a [inaudible 00:00:13].
David:	(laughs).  Who's this?
Child:	[inaudible 00:00:17].
David:	Good to meet you.  Good to meet you.  Have you ever seen one of these before?
Child:	Um, my favorite one is Jack and the Beanstalk.
David:	Jack and the Beanstalk?
Speaker 1:	We don't have one of those [inaudible 00:00:28].
Child:	A bird.
David:	Okay.
Speaker 1:	[inaudible 00:00:31] a bird [inaudible 00:00:31].
David:	Do you know what this thing is that I'm holding?
	Is it kind of like an iPad maybe?  Or [inaudible 00:00:39]?
	So you can touch things that you see on the screen.  You can [inaudible 00:00:49].  You can ... you can move things around.  Um, I'm going to ask you two to read together a story about a baby duck.  You can talk about the stuff that you see on the screen and touch the screen and ...
Child:	I ... I found ... I found a ... I found a tree.
Speaker 1:	You found a tree [inaudible 00:01:05]?
David:	Whoa.  (laughs).
Speaker 1:	(laughs).
David:	[inaudible 00:01:09] at any time.
Speaker 1:	Okay.
David:	[crosstalk 00:01:11].
Speaker 1:	All right.  Okay, what do you want to touch?
	[crosstalk 00:01:15].
	Tap, tap, tap, right?  [inaudible 00:01:19].
Child:	Tap, tap, tap.
Speaker 1:	[inaudible 00:01:26] happy.  Show me someone's there.  So [inaudible 00:01:33] maybe [inaudible 00:01:34].
Child:	[inaudible 00:01:36].
Speaker 1:	Yeah.  
	Help me, help me, help me get free.  Tell me, tell me what [inaudible 00:01:42].
	Do you see [inaudible 00:01:46]?
	What do you say?
	[crosstalk 00:01:49] to move something. What do you think?
	You are a baby duck and your name is [00:02:00] [Billy Dee 00:02:01].
	You can [inaudible 00:02:07], right?
Child:	I'm a baby duck.  I'm a baby [inaudible 00:02:11].
Speaker 1:	I've read all the words, so what do we do next?
Child:	Turn the page.
Speaker 1:	Yeah, turn the page.
	I'm a baby duck.  What colors am I [inaudible 00:02:22]?
Child:	Purple.
Speaker 1:	Purple?  [inaudible 00:02:27] rub, rub, rub.  Red, yellow, blue.  What color are you?
	Do you want to choose a color?  You can choose a color by just [inaudible 00:02:39].
	[crosstalk 00:02:43].
	Yes.
	What happened?  [inaudible 00:02:51 - 00:03:03]. 
	Where is he now?  Do you know?
Child:	[inaudible 00:03:05].
Speaker 1:	He's [inaudible 00:03:06].  I'm awake [inaudible 00:03:11].
	Well, [inaudible 00:03:15], I bet if you ... I bet if you [inaudible 00:03:17] those are the yellow circles.  You can find [inaudible 00:03:21].
	Yellow frog, yellow frog, take me away.
	Where do you think [inaudible 00:03:32]?  I want to play [inaudible 00:03:35].
Child:	[inaudible 00:03:38].
Speaker 1:	It [inaudible 00:03:39], yeah.  [inaudible 00:03:40] splash, dive, run [inaudible 00:03:43] go and have fun.  
	Hmm ... is there any place that looks like he ... he might ... he might have fun there?
Child:	[inaudible 00:03:51].
Speaker 1:	I am hungry.  What should I eat?  [inaudible 00:03:57].  What did you eat [inaudible 00:03:58] [00:04:00].  Oh wow, [inaudible 00:04:02] ten bugs?
	[inaudible 00:04:07].  I want more.  [inaudible 00:04:14].
Child:	More and more [inaudible 00:04:17].
Speaker 1:	How is he going to get [inaudible 00:04:21] more?  [inaudible 00:04:22]?
	Okay, we ...
Child:	[inaudible 00:04:25].
Speaker 1:	Nope, that one's [crosstalk 00:04:27] (laughs).
	My [inaudible 00:04:34] feels funny.  Why is that?  Why do his [inaudible 00:04:38] feel funny?
Child:	Well, he [crosstalk 00:04:40].
Speaker 1:	[inaudible 00:04:41].  What are we going to do about that?
Child:	[inaudible 00:04:47].
Speaker 1:	Make him a bed?  You're tired [inaudible 00:04:51].  It is your first big day.  Crash into bed, lay down your head.  
	I am a duck.  [inaudible 00:04:59] play and run all day [inaudible 00:05:01] just one thing more?
Child:	[crosstalk 00:05:08].
Speaker 1:	I think ... I think the other thing [inaudible 00:05:11] is a lullaby [inaudible 00:05:13] until a new day comes.  He's sound asleep.  Do you think he [inaudible 00:05:19]?  [inaudible 00:05:21]?  
Child:	[inaudible 00:05:24].
Speaker 1:	[inaudible 00:05:25].
Child:	I [inaudible 00:05:26].
Speaker 1:	You [inaudible 00:05:28]. 
David:	Can I give you a sticker? Do you want a sticker?  Ooo, thank you.  
	What did you think of the story?
Child:	Good.
David:	You liked it?  What was your favorite part?
Child:	Mm ... [inaudible 00:05:39].
David:	Yeah, [inaudible 00:05:41].  What was your least favorite part?
Child:	[inaudible 00:05:46].
David:	When they [inaudible 00:05:47]?  Do you have any suggestions on how I might improve the reading [inaudible 00:05:50]?
Speaker 1:	[inaudible 00:05:53]. I think that, um ... yeah, no ...
David:	[crosstalk 00:05:58].
Speaker 1:	... if [inaudible 00:05:58] done, [00:06:00] [inaudible 00:06:01] something that she would figure out to do, ...
David:	Yeah.
Speaker 1:	... you know, [inaudible 00:06:01] she would've figured it out if she did it a few times.
David:	Yeah, definitely.  [inaudible 00:06:05].
Speaker 1:	Um, and can it do ... can you set it so it will do the audio?
David:	Yeah [inaudible 00:06:10] as a matter of fact.  So ... so, we're ... we're actually studying how ... how the communication back and forth [inaudible 00:06:17].  But ... but there's ... there's a little bit of [inaudible 00:06:19].  
Speaker 1:	Oh, okay.
David:	But it really ... What we're really looking for is [inaudible 00:06:24] she used, but is it the way you asked questions about particular things.
Speaker 1:	Uh huh.
David:	Um, and what we're trying to do is see if we can improve apps like this in general by ... by [inaudible 00:06:33] cross [inaudible 00:06:34].
Speaker 1:	[inaudible 00:06:36] prompts ... prompts.
David:	So things like ... like when you asked ...
Speaker 1:	[inaudible 00:06:39].
David:	[crosstalk 00:06:40] or when you ask the question, you know, like, "What do you think happened?"  [inaudible 00:06:43] be more proactive [inaudible 00:06:46].
Speaker 1:	Uh huh, uh huh.
David:	So we're just ... we're watching the behaviors that you're exhibiting today and [inaudible 00:06:51].
Speaker 1:	How many ... how many of these are you ... I work ... I work on, like [inaudible 00:06:56] for adults ... I'm working on the app for the [inaudible 00:06:57].
David:	Oh cool.
Speaker 1:	[crosstalk 00:06:59].  Um, what kinds of ... what ... how many of these do you study during [inaudible 00:07:03]?
David:	Um, quite a [inaudible 00:07:05].  So we've, um ... we've ... this is one that we've developed.  We've ... we've actually had some [inaudible 00:07:09] Ethiopia and [inaudible 00:07:11].
Speaker 1:	Oh cool, mm-hmm (affirmative).
David:	Um, in those cases, we have really [inaudible 00:07:17] tablet type of [inaudible 00:07:18].
Speaker 1:	Uh huh.
David:	[inaudible 00:07:19].  Like ... like they're [inaudible 00:07:20] about a year and half now, so we know [inaudible 00:07:23].
Speaker 1:	Oh cool.  And is this [inaudible 00:07:23], or ...?
David:	One of them is the same, but there's also [inaudible 00:07:26].
Speaker 1:	Yeah.
David:	Um, so yeah, we're ... we're just kind of looking things that are sort of literacy [inaudible 00:07:35] in particular [inaudible 00:07:35].
Speaker 1:	[inaudible 00:07:38].
David:	[crosstalk 00:07:40] our study.
	You doing well?
Speaker 1:	Yeah, really well.
David:	So, did we [inaudible 00:07:50] our consent forms.  Is that ... like it's all educational opportunities [inaudible 00:07:54].
Speaker 1:	Okay, okay.
David:	Did we run out?  [inaudible 00:07:57]

