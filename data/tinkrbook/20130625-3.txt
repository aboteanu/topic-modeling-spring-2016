                                    20130625 3

                    [0:01] [background conversations]

  Parent:           [0:02] Tap.

  Tablet:           [0:04] Tap.

  Parent:           [0:06] Tap.

  Tablet:           [0:09] Tap. Tap. Tap.

  Parent:           [0:15] Help me! Help me! Help me be free. Tell me, tell me! What do
                    you see? What do you see? That's baby duck.

  Tablet:           [0:43] Baby D. Baby D.

  Child:            [0:47] Duck.

  Parent:           [0:49] Duck.

  Tablet:           [0:51] Baby D.

  Parent:           [0:52] Baby duck.

  Tablet:           [0:53] Baby D.

  Parent:           [0:55] I am a baby duck. What can I be? White. Yellow.

  Tablet:           [1:02] Duck.

  Child:            [1:05] Duck.

  Tablet:           [1:22] Duck. Duck. Duck. Duck. Duck. Duck.

                    [1:43] [background conversations]

  Parent:           [1:46] Let's try here. All right. Go to the tub. Rub, rub, rub.
                    Red, yellow, blue. What color are you? Where are you going? Yeah.
                    Yeah. Pick a color. I think I got out of the page accidentally.

                    [2:20] [background conversations]

  Parent:           [4:17] Forward. Forward. Forward. Forward. Here were are. Look at
                    the blue. See blue.

                    [4:28] Child. Wow.

                    [4:29] [crosstalk]

  Parent:           [4:32] ...Some other little icon...

                    [4:39] [crosstalk]

  Parent:           [inaudible 0:05:13] [5:13] where's the blue one?

  Child:            [inaudible 0:05:15] [5:15]

  Parent:           [5:17] Yellow, red, "azul," green. That's red.

  Child:            [inaudible 0:05:19] [5:19] ?

                    [5:22] [crosstalk]

  Parent:           [5:52] Can the ducky go swimming? Where's the "agua?" Yeah, that's
                    "agua." Where's the duck? Duck.

                    [5:59] [crosstalk]

  Parent:           [6:02] Where's the duck? [?] Water so the duck can go swimming.
                    Ooh. Swim, splash, dive, run. Baby Dee, go have fun. Baby Dee...

                    [6:13] [crosstalk]

  Researcher:       [6:30] All right.

  Parent:           [6:32] He's reaching his whole capacity.

  Researcher:       [6:33] Can I ask you a couple questions before you go?

  Parent:           [6:35] Yeah, sure.

  Researcher:       [6:36] All right. So, how old is he?

  Parent:           [6:37] Eighteen months.

  Researcher:       [6:39] What did you like about the story?

  Parent:           [6:42] Um...well, uh, it was cute. And, um, fortunately it, it
                    contained a duck, which he knows, and "agua," [inaudible 0:06:52] .

                    [6:55] [crosstalk]

  Parent:           [7:18] Ingram. Grinny. Grinny.

                    [7:22] [crosstalk]

                    [7:27] That's...he does exactly [inaudible 0:07:27] like I was
                    saying before.

  Woman 1:          [7:29] Grinny, you get a sticker.

  Researcher:       [7:31] You get a sticker. You get a sticker. Do you want a sticker?

  Parent:           [7:37] Come on.

  Researcher:       [laughs] [7:38] Do you want to put it on your lunch box? Grinny,
                    we'll put it on the lunch box? No? Oh, OK.

                    [7:46] No, no, no. Sorry

                    [7:51] [laughter]

                    [7:53] [crosstalk]

  Parent:           [8:18] Yeah, yeah, yeah. Um, what's the...like, what's, like, the
                    platform?

                    [8:35] [crosstalk]

  Parent:           [8:37] Oh, OK.

  Researcher:       [8:43] Yeah.

                    [8:55] Cool. Thanks.

  Parent:           [8:57] Thank you. It's a fun story. Congrats. Are you guys hoping
                    to publish this year, or...?

  Researcher:       [9:00] Um, well, this is like the first of two parts, so I'm not
                    sure if we'll get this one out this year or not. So we'll...Because
                    we have to make special robots that are [inaudible 0:09:08] with
                    kids around language learning as well, and hopefully this will
                    [inaudible 0:09:15] .

  Parent:           [9:15] Do you have, um, a website of your lab or anything?

  Researcher:       [9:17] Yeah, actually, I can give you a card.

  Parent:           [9:22] Because I'm actually going to do a fellowship in
                    developmental behavioral...

  Researcher:       [9:26] Yeah.

  Parent:           [9:27] ...pediatrics, so this is...that's why I was like, "Wow,
                    this is totally up my alley." So, Media Lab.

  Researcher:       [9:32] Yeah, so that's my card, and then we're at the Media Lab.

  Parent:           [9:35] Mm-hmm.

  Researcher:       [9:36] Um, our group's website...

  Parent:           [9:37] Personal robots.

  Researcher:       [9:38] ...currently being redone, or hopefully being redone, so
                    it's not up to date, because it was [inaudible 0:09:40] for a
                    while, but [inaudible 0:09:41] it has its own information.

  Parent:           [9:42] This one? Robotic.media.mit?

  Researcher:       [9:44] Yes. That would be our group's website.

  Parent:           [9:46] OK. Wow. That's very interesting.

  Researcher:       [9:46] Good.

  Parent:           [9:49] OK. Thank you.



Transcription by CastingWords
 