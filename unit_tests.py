if __name__=='__main__':
	import doctest
	import sys

	import topics_semnet.conceptnet_api.conceptnet_api as conceptnet_api
	import topics_semnet.wordnet_api.wordnet_api as wordnet_api
	import topics_semnet.cyc_api.cyc_api as cyc_api
	import topics_semnet.topics.generate_topics_from_vocab as gen_topics

	print 'Testing ConceptNet'
	doctest.testmod( conceptnet_api )
	print 'Done'

	print 'Testing WordNet'
	doctest.testmod( wordnet_api )
	print 'Done'

	print 'Testing Cyc'
	doctest.testmod( cyc_api )
	print 'Done'

	print 'Testing Topics with Semantic Nets'
	doctest.testmod( gen_topics )
	print 'Done'
