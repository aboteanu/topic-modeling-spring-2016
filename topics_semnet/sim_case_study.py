if __name__=='__main__':
	from topics.topics import Topics

	from conceptnet_api.divisi_sim import concept_pair_sim
	from conceptnet_api.conceptnet_api import node_expansion as concept_node_expansion
	from wordnet_api.wordnet_path_sim import wordnet_pair_sim
	from wordnet_api.wordnet_api import node_expansion as wordnet_node_expansion
	from cyc_api.cyc_api import node_expansion as cyc_node_expansion

	t = set(['red','blue'])
	u = set(['purple','yellow'])

	t2 = set(['red','blue','yellow'])
	u2 = set(['purple','yellow'])

	print 'topic to topic similarity examples'

	print Topics.topic_similarity(t,u, concept_pair_sim)
	print Topics.topic_similarity(t2,u2, concept_pair_sim)

	print Topics.topic_similarity2(t,u, concept_pair_sim)
	print Topics.topic_similarity2(t2,u2, concept_pair_sim)

	print Topics.topic_similarity3(t,u, concept_pair_sim)
	print Topics.topic_similarity3(t2,u2, concept_pair_sim)

	print Topics.topic_similarity_centroid(t,u, concept_pair_sim)
	print Topics.topic_similarity_centroid(t2,u2, concept_pair_sim)

	print Topics.topic_similarity_2_word_topics(t,u, concept_pair_sim)
	print Topics.topic_similarity_2_word_topics(t2,u2, concept_pair_sim)

	print Topics.topic_similarity(t,u, wordnet_pair_sim)
	print Topics.topic_similarity(t2,u2, wordnet_pair_sim)

	print Topics.topic_similarity2(t,u, wordnet_pair_sim)
	print Topics.topic_similarity2(t2,u2, wordnet_pair_sim)

	print Topics.topic_similarity3(t,u, wordnet_pair_sim)
	print Topics.topic_similarity3(t2,u2, wordnet_pair_sim)

	print Topics.topic_similarity_centroid(t,u, wordnet_pair_sim)
	print Topics.topic_similarity_centroid(t2,u2, wordnet_pair_sim)

	print Topics.topic_similarity_2_word_topics(t,u, wordnet_pair_sim)
	print Topics.topic_similarity_2_word_topics(t2,u2, wordnet_pair_sim)

	print 'word to word similarity examples'

	print concept_pair_sim('red','blue')
	print wordnet_pair_sim('red','blue')

	print concept_pair_sim('hot','cold')
	print wordnet_pair_sim('hot','cold')

	print concept_pair_sim('car','tea')
	print wordnet_pair_sim('car','tea')
