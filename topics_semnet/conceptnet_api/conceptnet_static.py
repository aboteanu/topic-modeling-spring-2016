#API_URL = 'http://130.215.27.134:8084/data/5.3/c/en/'
#API_URL = 'http://127.0.0.1:8084/data/5.3/c/en/'
API_URL = 'http://conceptnet5.media.mit.edu/data/5.4/c/en/'

CN_RESULT_LIMIT = 300
MAX_CONCEPTS = 300

# 1 for 'fork' only
# 2 for 'carving fork'
NWORD_CONCEPTS = 1

conceptnet_all_relation_types = [u'/r/DerivedFrom', u'/r/IsA', u'/r/ConceptuallyRelatedTo', u'/r/RelatedTo', u'/r/InstanceOf', u'/r/PartOf', u'/r/SimilarTo', u'/r/MemberOf', u'/r/HasContext', u'/r/Synonym', u'/r/HasProperty', u'/r/Antonym', u'/r/ReceivesAction', u'/r/Causes', u'/r/UsedFor', u'/r/HasPrerequisite', u'/r/wordnet/adjectivePertainsTo', u'/r/MadeOf', u'/r/Entails', u'/r/AtLocation', u'/r/Attribute', u'/r/Desires', u'/r/NotIsA', u'/r/CapableOf', u'/r/HasA', u'/r/SymbolOf', u'/r/CausesDesire', u'/r/NotDesires', u'/r/MotivatedByGoal', u'/r/CreatedBy', u'/r/DefinedAs', u'/r/HasSubevent', u'/r/HasFirstSubevent', u'/r/HasLastSubevent', u'/r/SimilarSize', u'/r/LocatedNear', u'/r/wordnet/adverbPertainsTo', u'/r/NotHasProperty', u'/r/NotCapableOf', u'/r/Derivative', u'/r/InheritsFrom', u'/r/wordnet/participleOf', u'/r/NotHasA', u'/r/HasPainIntensity', u'/r/NotMadeOf', u'/r/HasPainCharacter']

conceptnet_all_relation_types.sort()
