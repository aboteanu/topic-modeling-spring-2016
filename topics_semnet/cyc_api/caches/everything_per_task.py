class EverythingPerTaskCache():
    def __init__(self):
        self.backingDict = {}

    def __getitem__(self, key):
        return self.backingDict[key]

    def __setitem__(self, key, value):
        self.backingDict[key] = value

    def __contains__(self, item):
        return item in self.backingDict
    
    def endOfTaskCleanup(self):
        self.backingDict.clear()
