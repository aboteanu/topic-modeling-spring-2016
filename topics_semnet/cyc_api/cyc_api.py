import itertools
import xml.etree.ElementTree
from caches.everything_per_task import EverythingPerTaskCache

cache = EverythingPerTaskCache()

def clear_cache():
	cache.endOfTaskCleanup()

idToWordsMap = {}
wordToIdMap = {}
idToRelatedIds = {}

def init():
	'''
	Initialize the id-words, word-id,id-ids mappings from the opencyc owl data file.
	'''
	e = xml.etree.ElementTree.parse('topics_semnet/cyc_api/data/owl-export-unversioned.owl').getroot()
	#e = xml.etree.ElementTree.parse('./cyc_api/data/test.owl').getroot()
	l = e._children

	for c in l:
		id = c.attrib.get('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about')
		if id == None or len(id) == 0 or id.__contains__('http'):
			continue
		if idToWordsMap.get(id) != None:
			continue
		cl = c._children
		relatedIds = set()
		words = set()
		for x in cl:
			langAttr = x.attrib.get('{http://www.w3.org/XML/1998/namespace}lang')
			resourceAttr = x.attrib.get('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource')
			if x.tag != '{http://sw.cyc.com/CycAnnotations_v1#}label' \
					and x.tag != '{http://www.w3.org/2000/01/rdf-schema#}comment' \
					and langAttr == 'en':
				#print x.text
				words.add(x.text)
			elif resourceAttr != None and len(resourceAttr) > 0 and not resourceAttr.__contains__('http'):
				#print resourceAttr
				relatedIds.add(resourceAttr)
			idToWordsMap[id] = words
			idToRelatedIds[id] = relatedIds
		for w in words:
			wordToIdMap[w] = id

def node_expansion(x):
	'''
	Return all the words that the related ids refer to

	>>> assert len(node_expansion('red')) >= 0
	'''
	if x in cache:
		data = cache[x]
		return data
	else:
		if wordToIdMap.__contains__(x):
			id = wordToIdMap[x]
			sameMeaningWords = idToWordsMap[id]
			relatedIds = idToRelatedIds[id]
			words = map(lambda x, idToWordsMap=idToWordsMap: idToWordsMap[x] \
				if idToWordsMap.__contains__(x) else None, relatedIds)
			words += list(sameMeaningWords)
			words = map(lambda x:x if x != None and x.__len__() > 0 else [], words)
			words = filter(None, words)
			# merge all subsets from words into a single set
			result = set()
			for x in words:
				if type(x)==str:
					result.add(x)
				elif type(x)==set:
					result=result.union(x)
				else:
					raise Exception
			cache[x] = result
			return result
		cache[x] = set()
		return set()

if __name__=='__main__':
	import doctest
	doctest.testmod()
