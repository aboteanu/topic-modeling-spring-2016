from copy import deepcopy
from itertools import combinations
from itertools import product
import numpy as np

class Topics:
	'''
	Group words starting from a vocabulary
	Core of IUI 2013 paper
	Topic overlap from FSS 2013
	'''
	def __init__(self, vocab, pair_sim, node_expansion):
		# vocab is a set of lemmas
		self.vocab = vocab
		self.pair_sim = pair_sim
		self.node_expansion = node_expansion
		self.raw_topics = []
		self.refined_topics = []
		self.min_similarity = 0.3
		self.min_overlap = 0.5
		self.refine_depth = 2

	def raw(self):
		vocab = deepcopy(self.vocab)
		# seed first topic with a word
		t1 = set()
		t1.add( vocab.pop() )
		self.raw_topics.append(t1)
		for w in vocab:
			make_new_topic = True
			for t in self.raw_topics:
				matched = 0.0
				for tw in t:
					if self.pair_sim(w,tw)>self.min_similarity:
						matched = matched + 1
				if matched>=(self.min_overlap*len(t)):
					t.add(w)
					make_new_topic = False
			if make_new_topic:
				new_topic = set()
				new_topic.add(w)
				self.raw_topics.append(new_topic)

	@staticmethod
	def belongs_to_topic( t, x, depth , node_expansion):
		'''
		Return true if there is any intersection between the topic t
		And the depth-vicinity of node x
		'''
		if depth==0:
			return x in t

		candidates = Topics.expand_node_depth(x,depth, node_expansion)
		if len(candidates.intersection(set(t)))>0:
			return True
		return False

	@staticmethod
	def expand_node_depth( w, depth, node_expansion):
		'''
		BFS for depth steps
		'''
		explored=set()
		candidates=set([w])

		while depth>0:
			candidates1 = set()
			explored1 = set()
			for x in candidates.difference(explored):
				neighbors = node_expansion(x)
				candidates1=candidates1.union(neighbors)
				explored1.add(x)
			candidates = candidates.union(candidates1)
			explored = explored.union(explored1)
			depth = depth-1
		return candidates

	def refine(self):
		for t in self.raw_topics:
			refined = []
			tt = deepcopy(t)
			while len(tt)>0:
				x = tt.pop()
				split=True
				for y in refined:
					if Topics.belongs_to_topic(y, x, self.refine_depth, self.node_expansion)==True:
						y.add(x)
						split = False
						break
				if split==True:
					new_topic = set()
					new_topic.add(x)
					refined.append(new_topic)
			self.refined_topics += refined

	@staticmethod
	def topic_similarity(t, u, pair_sim):
		return Topics.set_overlap(t,u) + (1-Topics.set_overlap(t,u)) * Topics.words_avg_sim( t.difference(u).union(u.difference(t)), pair_sim)

	@staticmethod
	def topic_similarity2(t, u, pair_sim):
		return Topics.set_overlap(t,u) + (1-Topics.set_overlap(t,u)) * Topics.words_avg_sim_t2t( t.difference(t.intersection(u)),u.difference(t.intersection(u)), pair_sim)

	@staticmethod
	def topic_similarity3(t, u, pair_sim):
		return Topics.words_avg_sim_t2t( t, u, pair_sim)


	@staticmethod
	def topic_similarity_centroid(t, u, pair_sim):
		centroidT = Topics.find_centroid(t, pair_sim)
		centroidU = Topics.find_centroid(u, pair_sim)
		#print 'centroidT'
		#print centroidT
		#print 'centroidU'
		#print centroidU
		return pair_sim(centroidT,centroidU)

	@staticmethod
	def topic_similarity_2_word_topics(t, u, pair_sim):
		wordsT = Topics.find_2_word_max_sim_topic(t, pair_sim)
		wordsU = Topics.find_2_word_max_sim_topic(u, pair_sim)
		#print 'wordsT'
		#print wordsT
		#print 'wordsU'
		#print wordsU
		return Topics.words_avg_sim(wordsT.union(wordsU), pair_sim)

	@staticmethod
	def find_2_word_max_sim_topic(words, pair_sim):
		if not words:
			return None
		max_sim_pair = [list(words)[0]]
		max_sim = -1
		for w1,w2 in combinations(words,2):
			try:
				sim = pair_sim(w1,w2)
				if sim!=None and sim >= max_sim:
					max_sim = sim
					max_sim_pair = [w1,w2]
			except:
				continue
		return set(max_sim_pair)

	@staticmethod
	def find_centroid(words, pair_sim):
		if not words:
			return None
		maxAvgSqSim = -1
		centroid = list(words)[0]
		for w1 in words:
			vals = []
			for w2 in words:
				if w2 == w1:
					continue
				try:
					sim = pair_sim(w1,w2)
					if sim!=None and sim >= 0:
						vals.append( sim )
					else:
						vals.append( 0.0 )
				except:
					continue
			if not vals:
				avgSqSim = 0
			else:
				avgSqSim = sum(np.square(vals))/len(vals)
			if maxAvgSqSim <= avgSqSim:
				centroid = w1
				maxAvgSqSim = avgSqSim
		return centroid

	@staticmethod
	def words_avg_sim_t2t(t, u, pair_sim):
		if not t or not u:
			return 0.0
		vals = []
		for w1,w2 in product(t,u):
			try:
				sim = pair_sim(w1,w2)

				if sim!=None and sim >= 0:
					vals.append( sim )
				else:
					vals.append( 0.0 )
			except:
				continue
		if not vals:
			return 0.0
		return sum(vals)/len(vals)

	@staticmethod
	def words_avg_sim(words, pair_sim):
		if not words:
			return 0.0
		vals = []
		for w1,w2 in combinations(words,2):
			try:
				sim = pair_sim(w1,w2)

				if sim!=None and sim >= 0:
					vals.append( sim )
				else:
					vals.append( 0.0 )
			except:
				continue
		if not vals:
			return 0.0
		return sum(vals)/len(vals)
	
	@staticmethod
	def set_overlap(t, u):
		'''
		Return ratio of size of intersection to the size of the smallest topic
		Only match identical concepts between topics
		>>> t = {'red','blue','green'}
		>>> u = {'blue', 'sad'}
		>>> Topics.set_overlap(t,u)
		0.5
		>>> Topics.set_overlap(set(),t)
		0.0
		'''
		if not t or not u:
			return 0.0
		else:
			return float(len(t.intersection(u)))/(float(min(len(t),len(u))))
