from topics import Topics
from itertools import combinations
from topics_semnet.conceptnet_api.divisi_sim import concept_pair_sim
from topics_semnet.conceptnet_api.conceptnet_api import node_expansion as concept_node_expansion
from topics_semnet.wordnet_api.wordnet_path_sim import wordnet_pair_sim
from topics_semnet.wordnet_api.wordnet_api import node_expansion as wordnet_node_expansion
from topics_semnet.cyc_api.cyc_api import node_expansion as cyc_node_expansion

def generate_topics_from_vocab( vocabulary , pair_sim, node_expansion):
	'''
	Input a vocabylary as a set of words
	Output a list of sets, each being a topic

	This assumes all input is cleaned:
		words are all relevant e.g. no stopwords
		words all all in lemma form 

	>>> testvocab = set(['red', 'blue', 'green'])
	>>> topics = generate_topics_from_vocab( testvocab, pair_sim=concept_pair_sim, node_expansion=concept_node_expansion)
	>>> print topics
	[set(['blue', 'green', 'red'])]
	>>> testvocab = set(['red', 'blue', 'spatula'])
	>>> topics = generate_topics_from_vocab( testvocab, pair_sim=concept_pair_sim, node_expansion=concept_node_expansion)
	>>> print topics
	[set(['blue', 'red']), set(['spatula'])]
	>>> testvocab = set(['red', 'blue', 'green'])
	>>> topics = generate_topics_from_vocab( testvocab, pair_sim=wordnet_pair_sim, node_expansion=wordnet_node_expansion)
	>>> print topics
	[set(['blue', 'green', 'red'])]
	>>> testvocab = set(['red', 'blue', 'spatula'])
	>>> topics = generate_topics_from_vocab( testvocab, pair_sim=wordnet_pair_sim, node_expansion=wordnet_node_expansion)
	>>> print topics
	[set(['blue', 'red']), set(['spatula'])]
	>>> testvocab = set(['red', 'blue', 'green'])
	>>> topics = generate_topics_from_vocab( testvocab, pair_sim=concept_pair_sim, node_expansion=cyc_node_expansion)
	>>> print topics
	[set(['blue', 'green', 'red'])]
	>>> testvocab = set(['red', 'blue', 'spatula'])
	>>> topics = generate_topics_from_vocab( testvocab, pair_sim=concept_pair_sim, node_expansion=cyc_node_expansion)
	>>> print topics
	[set(['blue', 'red']), set(['spatula'])]
	'''

	topics = None

	if len(vocabulary)==0:
		return None

	t = Topics( vocab=vocabulary, pair_sim=pair_sim, node_expansion=node_expansion )

	t.raw()

	t.refine()

	return t.refined_topics
