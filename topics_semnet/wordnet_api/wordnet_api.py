import itertools
from caches.everything_per_task import EverythingPerTaskCache
from nltk.corpus import wordnet as wn

cache = EverythingPerTaskCache()

def clear_cache():
	cache.endOfTaskCleanup()

def node_expansion( x ):
	'''
	First check the cache, if not there go to WordNet

	>>> assert len(node_expansion('water bottle')) > 0
	'''
	# replace spaces with _ for stuff like "dinner plate" -> "dinner_plate"
	word = '_'.join( x.split(' ') )

	if word in cache:
		data = cache[word]
		return data
	else:
		data = _node_expansion( word )
		cache[ word ] = data
		return cache[ word ]

def _node_expansion(x):
	'''
	Return all the words found in the related synsets, hyponyms and hypernyms

	>>> assert len(node_expansion('water bottle')) > 0
	'''
	if x in cache:
		data = cache[x]
		return data
	else:
		relatedSynsets = list()

		synsets = wn.synsets(x)
		relatedSynsets += synsets
		relatedSynsets += list(itertools.chain(*map(lambda s: s.hyponyms(), synsets)))
		relatedSynsets += list(itertools.chain(*map(lambda s: s.hypernyms(), synsets)))
		related = set(list(itertools.chain(*map(lambda s: s.lemma_names(), relatedSynsets))))
		cache[x] = related
		return related

if __name__=='__main__':
	import doctest
	doctest.testmod()
